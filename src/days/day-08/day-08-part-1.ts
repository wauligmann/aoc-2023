import { readLines } from '../../utils/file';

const lines = await readLines('day-08', 'input');

enum Dir {
  Left = 'L',
  Right = 'R',
}

type Node = {
  left: string;
  right: string;
};

type Network = Map<string, Node>;

type Document = {
  instructions: Dir[];
  network: Network;
};

function parseDocument(lines: string[]): Document {
  const [instructions, _, ...nodes] = lines;
  return {
    instructions: instructions.split('') as Dir[],
    network: nodes.reduce((network, node) => {
      const [_, name, left, right] = node.match(/(\w+) = \((\w+), (\w+)\)/)!;
      return network.set(name, { left, right });
    }, new Map<string, Node>()),
  };
}

const document = parseDocument(lines);

function steps(from: string, to: string): number {
  let steps = 0;
  let current = from;
  while (current !== to) {
    const node = document.network.get(current)!;
    const dir = document.instructions[steps % document.instructions.length];
    current = dir === Dir.Left ? node.left : node.right;
    steps++;
  }
  return steps;
}

console.log(steps('AAA', 'ZZZ'));

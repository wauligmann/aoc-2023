import { last } from '../../utils/array';
import { readLines } from '../../utils/file';
import { exhaustiveCheck } from '../../utils/typescript';

const lines = await readLines('day-16', 'input');

enum Tile {
  EMPTY = '.',
  UPWARD_MIRROR = '/',
  DOWNWARD_MIRROR = '\\',
  VERTICAL_SPLITTER = '|',
  HORIZONTAL_SPLITTER = '-',
}

type Grid = Tile[][];

enum Direction {
  UP = 'UP',
  DOWN = 'DOWN',
  LEFT = 'LEFT',
  RIGHT = 'RIGHT',
}

type Point = {
  y: number;
  x: number;
};

type Beam = {
  rays: Ray[];
  done: boolean;
};

type Ray = {
  point: Point;
  direction: Direction;
};

function parseGrid(lines: string[]): Grid {
  return lines.map(line => line.split('') as Tile[]);
}

function tileAt(point: Point): Tile {
  return grid[point.y][point.x];
}

function hashRay(ray: Ray): string {
  return `${ray.point.y},${ray.point.x},${ray.direction}`;
}

function hashPoint(point: Point): string {
  return `${point.y},${point.x}`;
}

function move(ray: Ray): Ray {
  const moved = structuredClone(ray);

  switch (ray.direction) {
    case Direction.UP:
      moved.point.y -= 1;
      break;
    case Direction.DOWN:
      moved.point.y += 1;
      break;
    case Direction.LEFT:
      moved.point.x -= 1;
      break;
    case Direction.RIGHT:
      moved.point.x += 1;
      break;
    default:
      return exhaustiveCheck('Unhandled direction', ray.direction);
  }

  return moved;
}

function mirror(ray: Ray, mirror: Tile.UPWARD_MIRROR | Tile.DOWNWARD_MIRROR): Ray {
  switch (mirror) {
    case Tile.UPWARD_MIRROR:
      switch (ray.direction) {
        case Direction.UP:
          return {
            point: { y: ray.point.y, x: ray.point.x + 1 },
            direction: Direction.RIGHT,
          };
        case Direction.DOWN:
          return {
            point: { y: ray.point.y, x: ray.point.x - 1 },
            direction: Direction.LEFT,
          };
        case Direction.LEFT:
          return {
            point: { y: ray.point.y + 1, x: ray.point.x },
            direction: Direction.DOWN,
          };
        case Direction.RIGHT:
          return {
            point: { y: ray.point.y - 1, x: ray.point.x },
            direction: Direction.UP,
          };
        default:
          return exhaustiveCheck('Unhandled direction', ray.direction);
      }
    case Tile.DOWNWARD_MIRROR:
      switch (ray.direction) {
        case Direction.UP:
          return {
            point: { y: ray.point.y, x: ray.point.x - 1 },
            direction: Direction.LEFT,
          };
        case Direction.DOWN:
          return {
            point: { y: ray.point.y, x: ray.point.x + 1 },
            direction: Direction.RIGHT,
          };
        case Direction.LEFT:
          return {
            point: { y: ray.point.y - 1, x: ray.point.x },
            direction: Direction.UP,
          };
        case Direction.RIGHT:
          return {
            point: { y: ray.point.y + 1, x: ray.point.x },
            direction: Direction.DOWN,
          };
        default:
          return exhaustiveCheck('Unhandled direction', ray.direction);
      }
  }
}

function split(
  ray: Ray,
  splitter: Tile.VERTICAL_SPLITTER | Tile.HORIZONTAL_SPLITTER,
): [Ray] | [Ray, Ray] {
  switch (splitter) {
    case Tile.VERTICAL_SPLITTER:
      switch (ray.direction) {
        case Direction.UP:
          return [
            {
              point: { y: ray.point.y - 1, x: ray.point.x },
              direction: Direction.UP,
            },
          ];
        case Direction.DOWN:
          return [
            {
              point: { y: ray.point.y + 1, x: ray.point.x },
              direction: Direction.DOWN,
            },
          ];
        case Direction.LEFT:
        case Direction.RIGHT:
          return [
            {
              point: { y: ray.point.y - 1, x: ray.point.x },
              direction: Direction.UP,
            },
            {
              point: { y: ray.point.y + 1, x: ray.point.x },
              direction: Direction.DOWN,
            },
          ];
        default:
          return exhaustiveCheck('Unhandled direction', ray.direction);
      }
    case Tile.HORIZONTAL_SPLITTER:
      switch (ray.direction) {
        case Direction.LEFT:
          return [
            {
              point: { y: ray.point.y, x: ray.point.x - 1 },
              direction: Direction.LEFT,
            },
          ];
        case Direction.RIGHT:
          return [
            {
              point: { y: ray.point.y, x: ray.point.x + 1 },
              direction: Direction.RIGHT,
            },
          ];
        case Direction.UP:
        case Direction.DOWN:
          return [
            {
              point: { y: ray.point.y, x: ray.point.x - 1 },
              direction: Direction.LEFT,
            },
            {
              point: { y: ray.point.y, x: ray.point.x + 1 },
              direction: Direction.RIGHT,
            },
          ];
        default:
          return exhaustiveCheck('Unhandled direction', ray.direction);
      }
  }
}

function castBeam(beam: Beam): Beam[] {
  if (beam.done) {
    return [beam];
  }

  const head = last(beam.rays);
  const tile = tileAt(head.point);

  seenSet.add(hashRay(head));
  energizedSet.add(hashPoint(head.point));

  switch (tile) {
    case Tile.EMPTY:
      return appendRay(beam, move(head));
    case Tile.UPWARD_MIRROR:
    case Tile.DOWNWARD_MIRROR:
      return appendRay(beam, mirror(head, tile));
    case Tile.VERTICAL_SPLITTER:
    case Tile.HORIZONTAL_SPLITTER:
      return split(head, tile).flatMap(ray => appendRay(beam, ray));
    default:
      return exhaustiveCheck('Unhandled tile', tile);
  }
}

function appendRay(beam: Beam, ray: Ray): Beam[] {
  if (
    ray.point.y < 0 ||
    ray.point.y >= grid.length ||
    ray.point.x < 0 ||
    ray.point.x >= grid[0].length ||
    seenSet.has(hashRay(ray))
  ) {
    return [{ ...beam, done: true }];
  }
  return [
    {
      rays: [...beam.rays, ray],
      done: false,
    },
  ];
}

function castBeamsUntilNoChange(beams: Beam[]): Beam[] {
  while (!beams.every(beam => beam.done)) {
    beams = beams.flatMap(castBeam);
  }
  return beams;
}

const grid = parseGrid(lines);

const seenSet = new Set<string>();
const energizedSet = new Set<string>();

castBeamsUntilNoChange([
  {
    rays: [
      {
        point: { y: 0, x: 0 },
        direction: Direction.RIGHT,
      },
    ],
    done: false,
  },
]);

console.log(energizedSet.size);

import { last, range } from '../../utils/array';
import { readLines } from '../../utils/file';
import { max } from '../../utils/math';
import { exhaustiveCheck } from '../../utils/typescript';

const lines = await readLines('day-16', 'input');

enum Tile {
  EMPTY = '.',
  UPWARD_MIRROR = '/',
  DOWNWARD_MIRROR = '\\',
  VERTICAL_SPLITTER = '|',
  HORIZONTAL_SPLITTER = '-',
}

type Grid = Tile[][];

enum Direction {
  UP = 'UP',
  DOWN = 'DOWN',
  LEFT = 'LEFT',
  RIGHT = 'RIGHT',
}

type Point = {
  y: number;
  x: number;
};

type Beam = {
  rays: Ray[];
  done: boolean;
};

type Ray = {
  point: Point;
  direction: Direction;
};

function parseGrid(lines: string[]): Grid {
  return lines.map(line => line.split('') as Tile[]);
}

function tileAt(point: Point): Tile {
  return grid[point.y][point.x];
}

function hashRay(ray: Ray): string {
  return `${ray.point.y},${ray.point.x},${ray.direction}`;
}

function hashPoint(point: Point): string {
  return `${point.y},${point.x}`;
}

function move(ray: Ray): Ray {
  const moved = structuredClone(ray);

  switch (ray.direction) {
    case Direction.UP:
      moved.point.y -= 1;
      break;
    case Direction.DOWN:
      moved.point.y += 1;
      break;
    case Direction.LEFT:
      moved.point.x -= 1;
      break;
    case Direction.RIGHT:
      moved.point.x += 1;
      break;
    default:
      return exhaustiveCheck('Unhandled direction', ray.direction);
  }

  return moved;
}

function mirror(ray: Ray, mirror: Tile.UPWARD_MIRROR | Tile.DOWNWARD_MIRROR): Ray {
  switch (mirror) {
    case Tile.UPWARD_MIRROR:
      switch (ray.direction) {
        case Direction.UP:
          return {
            point: { y: ray.point.y, x: ray.point.x + 1 },
            direction: Direction.RIGHT,
          };
        case Direction.DOWN:
          return {
            point: { y: ray.point.y, x: ray.point.x - 1 },
            direction: Direction.LEFT,
          };
        case Direction.LEFT:
          return {
            point: { y: ray.point.y + 1, x: ray.point.x },
            direction: Direction.DOWN,
          };
        case Direction.RIGHT:
          return {
            point: { y: ray.point.y - 1, x: ray.point.x },
            direction: Direction.UP,
          };
        default:
          return exhaustiveCheck('Unhandled direction', ray.direction);
      }
    case Tile.DOWNWARD_MIRROR:
      switch (ray.direction) {
        case Direction.UP:
          return {
            point: { y: ray.point.y, x: ray.point.x - 1 },
            direction: Direction.LEFT,
          };
        case Direction.DOWN:
          return {
            point: { y: ray.point.y, x: ray.point.x + 1 },
            direction: Direction.RIGHT,
          };
        case Direction.LEFT:
          return {
            point: { y: ray.point.y - 1, x: ray.point.x },
            direction: Direction.UP,
          };
        case Direction.RIGHT:
          return {
            point: { y: ray.point.y + 1, x: ray.point.x },
            direction: Direction.DOWN,
          };
        default:
          return exhaustiveCheck('Unhandled direction', ray.direction);
      }
  }
}

function split(
  ray: Ray,
  splitter: Tile.VERTICAL_SPLITTER | Tile.HORIZONTAL_SPLITTER,
): [Ray] | [Ray, Ray] {
  switch (splitter) {
    case Tile.VERTICAL_SPLITTER:
      switch (ray.direction) {
        case Direction.UP:
          return [
            {
              point: { y: ray.point.y - 1, x: ray.point.x },
              direction: Direction.UP,
            },
          ];
        case Direction.DOWN:
          return [
            {
              point: { y: ray.point.y + 1, x: ray.point.x },
              direction: Direction.DOWN,
            },
          ];
        case Direction.LEFT:
        case Direction.RIGHT:
          return [
            {
              point: { y: ray.point.y - 1, x: ray.point.x },
              direction: Direction.UP,
            },
            {
              point: { y: ray.point.y + 1, x: ray.point.x },
              direction: Direction.DOWN,
            },
          ];
        default:
          return exhaustiveCheck('Unhandled direction', ray.direction);
      }
    case Tile.HORIZONTAL_SPLITTER:
      switch (ray.direction) {
        case Direction.LEFT:
          return [
            {
              point: { y: ray.point.y, x: ray.point.x - 1 },
              direction: Direction.LEFT,
            },
          ];
        case Direction.RIGHT:
          return [
            {
              point: { y: ray.point.y, x: ray.point.x + 1 },
              direction: Direction.RIGHT,
            },
          ];
        case Direction.UP:
        case Direction.DOWN:
          return [
            {
              point: { y: ray.point.y, x: ray.point.x - 1 },
              direction: Direction.LEFT,
            },
            {
              point: { y: ray.point.y, x: ray.point.x + 1 },
              direction: Direction.RIGHT,
            },
          ];
        default:
          return exhaustiveCheck('Unhandled direction', ray.direction);
      }
  }
}

function castBeam(
  beam: Beam,
  { seenSet, energizedSet }: { seenSet: Set<string>; energizedSet: Set<string> },
): Beam[] {
  if (beam.done) {
    return [beam];
  }

  const head = last(beam.rays);
  const tile = tileAt(head.point);

  seenSet.add(hashRay(head));
  energizedSet.add(hashPoint(head.point));

  switch (tile) {
    case Tile.EMPTY:
      return appendRay(beam, move(head), seenSet);
    case Tile.UPWARD_MIRROR:
    case Tile.DOWNWARD_MIRROR:
      return appendRay(beam, mirror(head, tile), seenSet);
    case Tile.VERTICAL_SPLITTER:
    case Tile.HORIZONTAL_SPLITTER:
      return split(head, tile).flatMap(ray => appendRay(beam, ray, seenSet));
    default:
      return exhaustiveCheck('Unhandled tile', tile);
  }
}

function appendRay(beam: Beam, ray: Ray, seenSet: Set<string>): Beam[] {
  if (
    ray.point.y < 0 ||
    ray.point.y >= grid.length ||
    ray.point.x < 0 ||
    ray.point.x >= grid[0].length ||
    seenSet.has(hashRay(ray))
  ) {
    return [{ ...beam, done: true }];
  }
  return [
    {
      rays: [...beam.rays, ray],
      done: false,
    },
  ];
}

function energizedTilesCount(beams: Beam[]): number {
  const seenSet = new Set<string>();
  const energizedSet = new Set<string>();

  while (!beams.every(beam => beam.done)) {
    beams = beams.flatMap(beam => castBeam(beam, { seenSet, energizedSet }));
  }

  return energizedSet.size;
}

function initialBeams(): [Beam][] {
  let beams: [Beam][] = [];

  beams = beams.concat(
    [0, grid.length - 1].flatMap(y => {
      return range(0, grid[0].length - 1).map(x => {
        return [
          {
            rays: [
              {
                point: { y, x },
                direction: y === 0 ? Direction.DOWN : Direction.UP,
              },
            ],
            done: false,
          },
        ] as [Beam];
      });
    }),
  );

  beams = beams.concat(
    [0, grid[0].length - 1].flatMap(x => {
      return range(0, grid.length - 1).map(y => {
        return [
          {
            rays: [
              {
                point: { y, x },
                direction: x === 0 ? Direction.RIGHT : Direction.LEFT,
              },
            ],
            done: false,
          },
        ] as [Beam];
      });
    }),
  );

  return beams;
}

const grid = parseGrid(lines);

console.log(max(initialBeams().map(energizedTilesCount)));

declare module '@graph-algorithm/minimum-cut' {
  export function mincut<T extends number | string, TEdge extends [T, T]>(
    edges: TEdge[],
  ): Generator<TEdge>;
}

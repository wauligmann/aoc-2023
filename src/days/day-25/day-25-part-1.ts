import { readLines } from '../../utils/file';
import { connectedComponents } from '../../utils/graph';
import { product } from '../../utils/math';
import { mincut } from '@graph-algorithm/minimum-cut';

const lines = await readLines('day-25', 'input');

type Component = string;

type Edge = [Component, Component];

function parseEdges(lines: string[]): Edge[] {
  return lines.flatMap(line => {
    const [component, connectedTo] = line.split(': ');
    return connectedTo.split(' ').map(connected => [component, connected] as Edge);
  });
}

function isEqual([a, b]: Edge, [c, d]: Edge): boolean {
  return (a === c && b === d) || (a === d && b === c);
}

const edges = parseEdges(lines);

for (const edge of mincut([...edges])) {
  edges.splice(
    edges.findIndex(e => isEqual(e, edge)),
    1,
  );
}

const partitions = connectedComponents(edges);

console.log(product(partitions.map(partition => partition.length)));

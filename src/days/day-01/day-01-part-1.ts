import { readLines } from '../../utils/file';
import { isDigit } from '../../utils/math';

const lines = await readLines('day-01', 'input');

const sum = lines.reduce((sum, line) => {
  const chars = line.split('');
  const firstDigit = chars.find(isDigit)!;
  const lastDigit = chars.findLast(isDigit)!;
  return sum + Number(firstDigit + lastDigit);
}, 0);

console.log(sum);

import { readLines } from '../../utils/file';
import { isDigit } from '../../utils/math';

const lines = await readLines('day-01', 'input');

const words = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];

function findFirstDigit(line: string): string | undefined {
  if (line.length === 0) return undefined;
  if (isDigit(line[0])) return line[0];
  if (words.some(word => line.startsWith(word))) {
    return String(words.findIndex(word => line.startsWith(word)) + 1);
  }
  return findFirstDigit(line.slice(1));
}

function findLastDigit(line: string): string | undefined {
  if (line.length === 0) return undefined;
  if (isDigit(line[line.length - 1])) return line[line.length - 1];
  if (words.some(word => line.endsWith(word))) {
    return String(words.findIndex(word => line.endsWith(word)) + 1);
  }
  return findLastDigit(line.slice(0, line.length - 1));
}

const sum = lines.reduce((sum, line) => {
  const firstDigit = findFirstDigit(line)!;
  const lastDigit = findLastDigit(line)!;
  return sum + Number(firstDigit + lastDigit);
}, 0);

console.log(sum);

import { readLines } from '../../utils/file';

const lines = await readLines('day-13', 'input');

enum Tile {
  Ash = '.',
  Rock = '#',
}

type Pattern = Tile[][];

type Direction = 'vertical' | 'horizontal';

type Reflection = {
  direction: Direction;
  index: number;
};

function parsePatterns(lines: string[]): Pattern[] {
  return lines
    .join('\n')
    .split('\n\n')
    .map(pattern => {
      return pattern.split('\n').map(line => {
        return line.split('') as Tile[];
      });
    });
}

function findReflection(pattern: Pattern): Reflection {
  const reflection = findHorizontalReflection(pattern) || findVerticalReflection(pattern);
  return reflection!;
}

function findHorizontalReflection(pattern: Pattern): Reflection | null {
  for (let i = 1; i < pattern.length; i++) {
    let allMirrored = true;
    for (let delta = 1; i - delta >= 0 && i + delta - 1 < pattern.length; delta++) {
      if (pattern[i - delta].join('') !== pattern[i + delta - 1].join('')) {
        allMirrored = false;
        break;
      }
    }
    if (allMirrored) {
      return {
        direction: 'horizontal',
        index: i,
      };
    }
  }
  return null;
}

function findVerticalReflection(pattern: Pattern): Reflection | null {
  for (let i = 1; i < pattern[0].length; i++) {
    let allMirrored = true;
    for (let delta = 1; i - delta >= 0 && i + delta - 1 < pattern[0].length; delta++) {
      const left = pattern.map(line => line[i - delta]).join('');
      const right = pattern.map(line => line[i + delta - 1]).join('');
      if (left !== right) {
        allMirrored = false;
        break;
      }
    }
    if (allMirrored) {
      return {
        direction: 'vertical',
        index: i,
      };
    }
  }
  return null;
}

function sumReflections(reflections: Reflection[]): number {
  return reflections.reduce((sum, reflection) => {
    switch (reflection.direction) {
      case 'vertical':
        return sum + reflection.index;
      case 'horizontal':
        return sum + 100 * reflection.index;
    }
  }, 0);
}

const patterns = parsePatterns(lines);

console.log(sumReflections(patterns.map(findReflection)));

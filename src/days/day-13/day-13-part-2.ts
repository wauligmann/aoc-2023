import { range } from '../../utils/array';
import { readLines } from '../../utils/file';

const lines = await readLines('day-13', 'input');

enum Tile {
  Ash = '.',
  Rock = '#',
}

type Pattern = Tile[][];

type Direction = 'vertical' | 'horizontal';

type Reflection = {
  direction: Direction;
  index: number;
};

function parsePatterns(lines: string[]): Pattern[] {
  return lines
    .join('\n')
    .split('\n\n')
    .map(pattern => {
      return pattern.split('\n').map(line => {
        return line.split('') as Tile[];
      });
    });
}

function otherTile(tile: Tile): Tile {
  switch (tile) {
    case Tile.Ash:
      return Tile.Rock;
    case Tile.Rock:
      return Tile.Ash;
  }
}

function findReflections(pattern: Pattern): Reflection[] {
  return [...findHorizontalReflections(pattern), ...findVerticalReflections(pattern)];
}

function findHorizontalReflections(pattern: Pattern): Reflection[] {
  const reflections: Reflection[] = [];
  for (let i = 1; i < pattern.length; i++) {
    let allMirrored = true;
    for (let delta = 1; i - delta >= 0 && i + delta - 1 < pattern.length; delta++) {
      if (pattern[i - delta].join('') !== pattern[i + delta - 1].join('')) {
        allMirrored = false;
        break;
      }
    }
    if (allMirrored) {
      reflections.push({
        direction: 'horizontal',
        index: i,
      });
    }
  }
  return reflections;
}

function findVerticalReflections(pattern: Pattern): Reflection[] {
  const reflections: Reflection[] = [];
  for (let i = 1; i < pattern[0].length; i++) {
    let allMirrored = true;
    for (let delta = 1; i - delta >= 0 && i + delta - 1 < pattern[0].length; delta++) {
      const left = pattern.map(line => line[i - delta]).join('');
      const right = pattern.map(line => line[i + delta - 1]).join('');
      if (left !== right) {
        allMirrored = false;
        break;
      }
    }
    if (allMirrored) {
      reflections.push({
        direction: 'vertical',
        index: i,
      });
    }
  }
  return reflections;
}

function permutations(pattern: Pattern): Pattern[] {
  return range(0, pattern.length - 1).flatMap(row => {
    return range(0, pattern[0].length - 1).map(col => {
      return pattern.with(row, pattern[row].with(col, otherTile(pattern[row][col])));
    });
  });
}

function findDifferentReflection(pattern: Pattern): Reflection {
  const reflection = findReflections(pattern)[0];

  for (const otherPattern of permutations(pattern)) {
    for (const otherReflection of findReflections(otherPattern)) {
      if (
        otherReflection.direction !== reflection.direction ||
        otherReflection.index !== reflection.index
      ) {
        return otherReflection;
      }
    }
  }

  throw new Error('No different reflection found');
}

function sumReflections(reflections: Reflection[]): number {
  return reflections.reduce((sum, reflection) => {
    switch (reflection.direction) {
      case 'vertical':
        return sum + reflection.index;
      case 'horizontal':
        return sum + 100 * reflection.index;
    }
  }, 0);
}

const patterns = parsePatterns(lines);

console.log(sumReflections(patterns.map(findDifferentReflection)));

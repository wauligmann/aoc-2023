import { sortBy, tally } from '../../utils/array';
import { readLines } from '../../utils/file';
import { sum } from '../../utils/math';
import { fromEntries } from '../../utils/object';

const lines = await readLines('day-07', 'input');

const cardNames = ['2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A'] as const;

type CardName = (typeof cardNames)[number];

const cardMap: Record<CardName, number> = fromEntries(
  cardNames.map((name, index) => [name, index]),
);

type Card = {
  name: CardName;
  value: number;
};

type Hand = {
  cards: Card[];
  bid: number;
};

enum HandType {
  FiveOfAKind = 7,
  FourOfAKind = 6,
  FullHouse = 5,
  ThreeOfAKind = 4,
  TwoPair = 3,
  OnePair = 2,
  HighCard = 1,
}

function parseHands(lines: string[]): Hand[] {
  return lines.map(line => {
    const [cards, bid] = line.split(' ');
    return {
      cards: cards.split('').map(card => ({
        name: card as CardName,
        value: cardMap[card as CardName],
      })),
      bid: Number(bid),
    };
  });
}

function getHandType(cards: Card[]): HandType {
  const counts = Object.values(tally(cards.map(card => card.name)));

  switch (true) {
    case counts.some(count => count === 5):
      return HandType.FiveOfAKind;
    case counts.some(count => count === 4):
      return HandType.FourOfAKind;
    case counts.some(count => count === 3) && counts.some(count => count === 2):
      return HandType.FullHouse;
    case counts.some(count => count === 3):
      return HandType.ThreeOfAKind;
    case counts.filter(count => count === 2).length === 2:
      return HandType.TwoPair;
    case counts.some(count => count === 2):
      return HandType.OnePair;
    default:
      return HandType.HighCard;
  }
}

function scoreHand(cards: Card[]): number[] {
  return [getHandType(cards), ...cards.map(card => card.value)];
}

function sortedHandsByRank(hands: Hand[]) {
  return sortBy(hands, hand => scoreHand(hand.cards), 'asc');
}

const hands = parseHands(lines);

const sortedHands = sortedHandsByRank(hands);

console.log(sum(sortedHands.map((hand, index) => hand.bid * (index + 1))));

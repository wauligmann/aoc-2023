import { readLines } from '../../utils/file';
import { sum } from '../../utils/math';

const lines = await readLines('day-15', 'input');

const NUM_BOXES = 256;

enum Operation {
  REMOVE = '-',
  ADD = '=',
}

type Step = {
  label: string;
  box: number;
} & (
  | {
      operation: Operation.REMOVE;
    }
  | {
      operation: Operation.ADD;
      focalLength: number;
    }
);

type Lens = {
  label: string;
  box: number;
  slot: number;
  focalLength: number;
};

type Box = Lens[];

const boxes: Box[] = new Array(NUM_BOXES).fill(0).map(() => []);

function parseSteps(lines: string[]): Step[] {
  return lines[0].split(',').map(step => {
    if (step.includes(Operation.REMOVE)) {
      const [label] = step.split(Operation.REMOVE);
      return {
        label,
        box: hash(label),
        operation: Operation.REMOVE,
      };
    } else {
      const [label, focalLength] = step.split(Operation.ADD);
      return {
        label,
        box: hash(label),
        operation: Operation.ADD,
        focalLength: Number(focalLength),
      };
    }
  });
}

function hash(label: string): number {
  return [...label].reduce((acc, char) => {
    acc += char.charCodeAt(0);
    acc *= 17;
    acc %= NUM_BOXES;
    return acc;
  }, 0);
}

function runSteps(steps: Step[]) {
  steps.forEach(step => {
    switch (step.operation) {
      case Operation.REMOVE:
        boxes[step.box] = boxes[step.box].filter(lens => lens.label !== step.label);
        boxes[step.box].forEach((lens, index) => (lens.slot = index));
        break;
      case Operation.ADD:
        const index = boxes[step.box].findIndex(lens => lens.label === step.label);
        if (index === -1) {
          boxes[step.box].push({
            label: step.label,
            box: step.box,
            slot: boxes[step.box].length,
            focalLength: step.focalLength,
          });
        } else {
          boxes[step.box][index].focalLength = step.focalLength;
        }
        break;
    }
  });
}

function focusingPower(lens: Lens): number {
  return (lens.box + 1) * (lens.slot + 1) * lens.focalLength;
}

const steps = parseSteps(lines);

runSteps(steps);

console.log(sum(boxes.flatMap(box => box.map(focusingPower))));

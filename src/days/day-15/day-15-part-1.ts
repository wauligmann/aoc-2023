import { readLines } from '../../utils/file';
import { sum } from '../../utils/math';

const lines = await readLines('day-15', 'input');

type Step = string;

function parseSteps(lines: string[]): Step[] {
  return lines[0].split(',');
}

function hash(step: Step): number {
  return [...step].reduce((acc, char) => {
    acc += char.charCodeAt(0);
    acc *= 17;
    acc %= 256;
    return acc;
  }, 0);
}

const steps = parseSteps(lines);

console.log(sum(steps.map(hash)));

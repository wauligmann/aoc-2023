import { chunkBy } from '../../utils/array';
import { readLines } from '../../utils/file';
import { product, sum } from '../../utils/math';
import { exhaustiveCheck } from '../../utils/typescript';

const lines = await readLines('day-19', 'input');

const INITIAL_WORKFLOW_NAME = 'in';

const MIN_RATING = 1;
const MAX_RATING = 4000;

const properties = ['x', 'm', 'a', 's'] as const;

type Property = (typeof properties)[number];

type Part = {
  [key in Property]: number;
};

type Branch = {
  minX?: number;
  maxX?: number;
  minM?: number;
  maxM?: number;
  minA?: number;
  maxA?: number;
  minS?: number;
  maxS?: number;
};

type Workflow = {
  name: string;
  rules: Rule[];
};

type Rule = {
  condition?: Condition;
  destination: Destination;
};

type Condition = {
  property: keyof Part;
  operator: Operator;
  value: Part[keyof Part];
};

enum Operator {
  LESS_THAN = '<',
  GREATER_THAN = '>',
}

type Destination =
  | {
      type: DestinationType.WORKFLOW;
      workflowName: Workflow['name'];
    }
  | {
      type: DestinationType.REJECT;
    }
  | {
      type: DestinationType.ACCEPT;
    };

enum DestinationType {
  WORKFLOW = 'W',
  REJECT = 'R',
  ACCEPT = 'A',
}

const CONDITION_REGEXP = new RegExp(`(\\w)([${Object.values(Operator).join('')}])(\\d+)`);

function parseWorkflows(lines: string[]): Workflow[] {
  const [workflows] = chunkBy(lines, line => line === '');

  function parseWorkflow(workflowLine: string): Workflow {
    const [name, rules] = workflowLine.split('{');
    return {
      name,
      rules: rules
        .slice(0, -1)
        .split(',')
        .map(rule => {
          const [destination, condition] = rule.split(':').reverse() as [
            string,
            string | undefined,
          ];
          if (condition) {
            const [property, operator, value] = condition.match(CONDITION_REGEXP)!.slice(1) as [
              keyof Part,
              Operator,
              string,
            ];
            return {
              condition: {
                property,
                operator,
                value: Number(value),
              },
              destination: parseDestination(destination),
            };
          } else {
            return { destination: parseDestination(destination) };
          }
        }),
    };
  }

  function parseDestination(word: string): Destination {
    switch (word) {
      case DestinationType.ACCEPT:
      case DestinationType.REJECT:
        return { type: word };
      default:
        return { type: DestinationType.WORKFLOW, workflowName: word };
    }
  }

  return workflows.map(parseWorkflow);
}

function applyCondition(branch: Branch, condition: Condition): [Branch, Branch] {
  const { property, operator, value } = condition;

  const maxProp = `max${property.toUpperCase()}` as keyof Branch;
  const minProp = `min${property.toUpperCase()}` as keyof Branch;

  switch (operator) {
    case Operator.LESS_THAN:
      return [
        {
          ...branch,
          [maxProp]: Math.min(branch[maxProp] ?? Infinity, value - 1),
        },
        {
          ...branch,
          [minProp]: Math.min(branch[maxProp] ?? Infinity, value),
        },
      ];
    case Operator.GREATER_THAN:
      return [
        {
          ...branch,
          [minProp]: Math.max(branch[minProp] ?? -Infinity, value + 1),
        },
        {
          ...branch,
          [maxProp]: Math.max(branch[minProp] ?? -Infinity, value),
        },
      ];
    default:
      exhaustiveCheck('Unhandeled operator', operator);
  }
}

function acceptedBranches(
  branch: Branch,
  workflowName = INITIAL_WORKFLOW_NAME,
  ruleIndex = 0,
): Branch[] {
  const workflow = workflows.find(w => w.name === workflowName)!;
  const rule = workflow.rules[ruleIndex];

  if (rule.condition) {
    const [fulfilled, unfulfilled] = applyCondition(branch, rule.condition);

    switch (rule.destination.type) {
      case DestinationType.ACCEPT:
        return [fulfilled, ...acceptedBranches(unfulfilled, workflowName, ruleIndex + 1)];
      case DestinationType.REJECT:
        return acceptedBranches(unfulfilled, workflowName, ruleIndex + 1);
      case DestinationType.WORKFLOW:
        return [
          ...acceptedBranches(fulfilled, rule.destination.workflowName, 0),
          ...acceptedBranches(unfulfilled, workflowName, ruleIndex + 1),
        ];
    }
  } else {
    switch (rule.destination.type) {
      case DestinationType.ACCEPT:
        return [branch];
      case DestinationType.REJECT:
        return [];
      case DestinationType.WORKFLOW:
        return acceptedBranches(branch, rule.destination.workflowName, 0);
    }
  }
}

function combinations(branch: Branch): number {
  const minX = branch.minX ?? MIN_RATING;
  const maxX = branch.maxX ?? MAX_RATING;
  const minM = branch.minM ?? MIN_RATING;
  const maxM = branch.maxM ?? MAX_RATING;
  const minA = branch.minA ?? MIN_RATING;
  const maxA = branch.maxA ?? MAX_RATING;
  const minS = branch.minS ?? MIN_RATING;
  const maxS = branch.maxS ?? MAX_RATING;

  const combinationsX = maxX - minX + 1;
  const combinationsM = maxM - minM + 1;
  const combinationsA = maxA - minA + 1;
  const combinationsS = maxS - minS + 1;

  return product([combinationsX, combinationsM, combinationsA, combinationsS]);
}

const workflows = parseWorkflows(lines);

const branches = acceptedBranches({});

console.log(sum(branches.map(combinations)));

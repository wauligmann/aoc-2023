import { chunkBy } from '../../utils/array';
import { readLines } from '../../utils/file';
import { sum } from '../../utils/math';

const lines = await readLines('day-19', 'input');

const INITIAL_WORKFLOW_NAME = 'in';

type Part = {
  x: number;
  m: number;
  a: number;
  s: number;
};

type Workflow = {
  name: string;
  rules: Rule[];
};

type Rule = {
  condition?: Condition;
  destination: Destination;
};

type Condition = {
  property: keyof Part;
  operator: Operator;
  value: Part[keyof Part];
};

enum Operator {
  LESS_THAN = '<',
  GREATER_THAN = '>',
}

type Destination =
  | {
      type: DestinationType.WORKFLOW;
      workflowName: Workflow['name'];
    }
  | {
      type: DestinationType.REJECT;
    }
  | {
      type: DestinationType.ACCEPT;
    };

enum DestinationType {
  WORKFLOW = 'W',
  REJECT = 'R',
  ACCEPT = 'A',
}

const CONDITION_REGEXP = new RegExp(`(\\w)([${Object.values(Operator).join('')}])(\\d+)`);

function parseWorkflowsAndParts(lines: string[]): [Workflow[], Part[]] {
  const [workflows, parts] = chunkBy(lines, line => line === '');

  function parseWorkflow(workflowLine: string): Workflow {
    const [name, rules] = workflowLine.split('{');
    return {
      name,
      rules: rules
        .slice(0, -1)
        .split(',')
        .map(rule => {
          const [destination, condition] = rule.split(':').reverse() as [
            string,
            string | undefined,
          ];
          if (condition) {
            const [property, operator, value] = condition.match(CONDITION_REGEXP)!.slice(1) as [
              keyof Part,
              Operator,
              string,
            ];
            return {
              condition: {
                property,
                operator,
                value: Number(value),
              },
              destination: parseDestination(destination),
            };
          } else {
            return { destination: parseDestination(destination) };
          }
        }),
    };
  }

  function parseDestination(word: string): Destination {
    switch (word) {
      case DestinationType.ACCEPT:
      case DestinationType.REJECT:
        return { type: word };
      default:
        return { type: DestinationType.WORKFLOW, workflowName: word };
    }
  }

  function parsePart(partLine: string): Part {
    const matches = partLine.match(/\d+/g)!;
    return {
      x: Number(matches[0]),
      m: Number(matches[1]),
      a: Number(matches[2]),
      s: Number(matches[3]),
    };
  }

  return [workflows.map(parseWorkflow), parts.map(parsePart)];
}

function processPart(part: Part): DestinationType.ACCEPT | DestinationType.REJECT {
  let currentWorkFlowName = INITIAL_WORKFLOW_NAME;

  while (true) {
    const workflow = workflows.find(workflow => workflow.name === currentWorkFlowName)!;
    const rule = workflow.rules.find(rule => {
      if (rule.condition) {
        const { property, operator, value } = rule.condition;
        switch (operator) {
          case Operator.LESS_THAN:
            return part[property] < value;
          case Operator.GREATER_THAN:
            return part[property] > value;
        }
      } else {
        return true;
      }
    })!;
    switch (rule.destination.type) {
      case DestinationType.ACCEPT:
      case DestinationType.REJECT:
        return rule.destination.type;
      case DestinationType.WORKFLOW:
        currentWorkFlowName = rule.destination.workflowName;
        break;
    }
  }
}

function partRating(part: Part): number {
  return sum(Object.values(part));
}

const [workflows, parts] = parseWorkflowsAndParts(lines);

const acceptedParts = parts.filter(part => processPart(part) === DestinationType.ACCEPT);

console.log(sum(acceptedParts.map(partRating)));

import { sortBy, unique } from '../../utils/array';
import { readLines } from '../../utils/file';
import { overlap } from '../../utils/geo';
import { sum } from '../../utils/math';

const lines = await readLines('day-22', 'input');

type Point = {
  x: number;
  y: number;
  z: number;
};

type BrickId = string; // A, ... Z, AA, ...

type Brick = {
  id: BrickId;
  bottom: Point;
  top: Point;
};

type SettledBrick = Brick & {
  supportedBy: BrickId[];
};

function parseBricks(lines: string[]): Brick[] {
  return lines.map((line, index) => {
    const [start, end] = line.split('~');
    const [x1, y1, z1] = start.split(',').map(Number);
    const [x2, y2, z2] = end.split(',').map(Number);
    const point1 = { x: x1, y: y1, z: z1 };
    const point2 = { x: x2, y: y2, z: z2 };
    return {
      id: brickId(index),
      bottom: z1 < z2 ? point1 : point2,
      top: z1 < z2 ? point2 : point1,
    };
  });
}

function brickId(index: number): string {
  let id = '';
  while (index >= 0) {
    id = String.fromCharCode('A'.charCodeAt(0) + (index % 26)) + id;
    index = Math.floor(index / 26) - 1;
  }
  return id;
}

function supportedBy(brick: Brick, bricks: SettledBrick[], z: number): SettledBrick[] {
  return bricks.filter(support => {
    return support.top.z === z && overlap([brick.bottom, brick.top], [support.bottom, support.top]);
  });
}

function settleBricks(bricks: Brick[]): SettledBrick[] {
  const sortedBricks = sortBy(bricks, brick => brick.bottom.z, 'asc');

  const settledBricks: SettledBrick[] = [];

  for (const brick of sortedBricks) {
    let settledZ = brick.bottom.z;
    let supportingBricks: SettledBrick[] = [];

    while (settledZ > 1) {
      supportingBricks = supportedBy(brick, settledBricks, settledZ - 1);

      if (supportingBricks.length) {
        break;
      }

      settledZ--;
    }

    settledBricks.push({
      ...brick,
      top: { ...brick.top, z: settledZ + (brick.top.z - brick.bottom.z) },
      bottom: { ...brick.bottom, z: settledZ },
      supportedBy: supportingBricks.map(brick => brick.id),
    });
  }

  return settledBricks;
}

function unsafeToDisintegrateBricks(bricks: SettledBrick[]): BrickId[] {
  return unique(bricks.flatMap(brick => (brick.supportedBy.length === 1 ? brick.supportedBy : [])));
}

function fallCount(brickIdToDisintegrate: BrickId, bricks: SettledBrick[]): number {
  let fallingBricks = structuredClone(bricks);
  let currentBrickIdsToDisintegrate = [brickIdToDisintegrate];
  let fallCount = 0;

  while (currentBrickIdsToDisintegrate.length > 0) {
    fallingBricks = fallingBricks.filter(
      brick => !currentBrickIdsToDisintegrate.includes(brick.id),
    );
    fallingBricks.forEach(brick => {
      brick.supportedBy = brick.supportedBy.filter(
        brickId => !currentBrickIdsToDisintegrate.includes(brickId),
      );
    });
    currentBrickIdsToDisintegrate = fallingBricks
      .filter(brick => brick.bottom.z > 1 && brick.supportedBy.length === 0)
      .map(brick => brick.id);
    fallCount += currentBrickIdsToDisintegrate.length;
  }

  return fallCount;
}

const bricks = parseBricks(lines);

const settledBricks = settleBricks(bricks);

const unsafeBricks = unsafeToDisintegrateBricks(settledBricks);

console.log(sum(unsafeBricks.map(brickId => fallCount(brickId, settledBricks))));

import { range, zip } from '../../utils/array';
import { readLines } from '../../utils/file';
import { product } from '../../utils/math';

const lines = await readLines('day-06', 'input');

type Race = {
  time: number;
  distance: number;
};

function parseRaces(lines: string[]): Race[] {
  const [timeLine, distanceLine] = lines;
  const toNumbers = (line: string) => line.split(':')[1].split(' ').filter(Boolean).map(Number);
  return zip(toNumbers(timeLine), toNumbers(distanceLine)).map(([time, distance]) => ({
    time,
    distance,
  }));
}

function winOptions(race: Race): number {
  return range(0, race.time)
    .map(chargeTime => {
      const travelTime = race.time - chargeTime;
      return travelTime * chargeTime;
    })
    .filter(distance => distance > race.distance).length;
}

const races = parseRaces(lines);

console.log(product(races.map(winOptions)));

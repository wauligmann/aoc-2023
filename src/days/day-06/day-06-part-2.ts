import { readLines } from '../../utils/file';

const lines = await readLines('day-06', 'input');

type Race = {
  time: number;
  distance: number;
};

function parseRace(lines: string[]): Race {
  const [time, distance] = lines;
  const toNumber = (line: string) => Number(line.split(':')[1].replaceAll(' ', ''));
  return {
    time: toNumber(time),
    distance: toNumber(distance),
  };
}

function winOptions(race: Race): number {
  const sqrt = Math.sqrt(race.time * race.time - 4 * race.distance);
  const lowerBound = Math.ceil((race.time - sqrt) / 2);
  const upperBound = Math.floor((sqrt + race.time) / 2);
  return upperBound - lowerBound + 1;
}

const race = parseRace(lines);

console.log(winOptions(race));

import { readLines } from '../../utils/file';

const lines = await readLines('day-21', 'input');

const STEPS = 64;

enum Tile {
  START = 'S',
  GARDEN = '.',
  ROCK = '#',
}

type Grid = Tile[][];

type Point = {
  y: number;
  x: number;
};

type Hash = string;

function parseGrid(lines: string[]): Grid {
  return lines.map(line => line.split('') as Tile[]);
}

function hash(point: Point): Hash {
  return `${point.y},${point.x}`;
}

function findStart(): Point {
  for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[y].length; x++) {
      if (grid[y][x] === Tile.START) {
        return { y, x };
      }
    }
  }
  throw new Error('Could not find start');
}

function tileAt(point: Point): Tile {
  return grid[point.y]?.[point.x] ?? Tile.ROCK;
}

function neighbors(point: Point): Point[] {
  if (tileAt(point) === Tile.ROCK) {
    throw new Error('Cannot get neighbors of a rock');
  }
  return [
    { y: point.y - 1, x: point.x },
    { y: point.y + 1, x: point.x },
    { y: point.y, x: point.x - 1 },
    { y: point.y, x: point.x + 1 },
  ].filter(point => tileAt(point) !== Tile.ROCK);
}

function walk(point: Point, steps: number): Set<Point> {
  let points = new Map<Hash, Point>([[hash(point), point]]);

  for (let step = 0; step < steps; step++) {
    let newPoints = new Map<Hash, Point>();

    for (const point of points.values()) {
      for (const neighbor of neighbors(point)) {
        newPoints.set(hash(neighbor), neighbor);
      }
    }

    points = newPoints;
  }

  return new Set(points.values());
}

const grid = parseGrid(lines);

console.log(walk(findStart(), STEPS).size);

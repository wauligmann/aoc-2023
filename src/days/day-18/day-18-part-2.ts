import { last } from '../../utils/array';
import { readLines } from '../../utils/file';
import { areaOfPolygon } from '../../utils/geo';
import { hexToDec } from '../../utils/math';
import { exhaustiveCheck } from '../../utils/typescript';

const lines = await readLines('day-18', 'input');

enum Direction {
  RIGHT = '0',
  DOWN = '1',
  LEFT = '2',
  UP = '3',
}

type Instruction = {
  direction: Direction;
  steps: number;
};

type Point = {
  y: number;
  x: number;
};

type Loop = {
  points: Point[];
  circumference: number;
};

function parseInstructions(lines: string[]): Instruction[] {
  return lines.map(line => {
    const color = last(line.split(' ')).slice(2, -1);
    return {
      direction: color[color.length - 1] as Direction,
      steps: hexToDec(color.slice(0, -1)),
    };
  });
}

function digLoop(instructions: Instruction[]): Loop {
  let currentPoint: Point = { x: 0, y: 0 };

  const loop: Loop = {
    points: [currentPoint],
    circumference: 0,
  };

  for (const instruction of instructions) {
    switch (instruction.direction) {
      case Direction.UP:
        currentPoint.y -= instruction.steps;
        break;
      case Direction.DOWN:
        currentPoint.y += instruction.steps;
        break;
      case Direction.LEFT:
        currentPoint.x -= instruction.steps;
        break;
      case Direction.RIGHT:
        currentPoint.x += instruction.steps;
        break;
      default:
        exhaustiveCheck('Unhandled direction', instruction.direction);
    }
    loop.points.push({ ...currentPoint });
    loop.circumference += instruction.steps;
  }

  return loop;
}

function capacity(loop: Loop): number {
  return areaOfPolygon(loop.points) + loop.circumference / 2 + 1;
}

const instructions = parseInstructions(lines);

const loop = digLoop(instructions);

console.log(capacity(loop));

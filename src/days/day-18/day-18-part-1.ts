import { readLines } from '../../utils/file';
import { pointInPolygon, pointToString } from '../../utils/geo';
import { exhaustiveCheck } from '../../utils/typescript';

const lines = await readLines('day-18', 'input');

enum Direction {
  UP = 'U',
  DOWN = 'D',
  LEFT = 'L',
  RIGHT = 'R',
}

type Instruction = {
  direction: Direction;
  steps: number;
  color: string;
};

type Point = {
  y: number;
  x: number;
};

type Loop = Point[];

function parseInstructions(lines: string[]): Instruction[] {
  return lines.map(line => {
    const [direction, steps, color] = line.split(' ');
    return {
      direction: direction as Direction,
      steps: Number(steps),
      color: color.slice(1, -1),
    };
  });
}

function digLoop(instructions: Instruction[]): Loop {
  let currentPoint: Point = { x: 0, y: 0 };

  const loop: Loop = [currentPoint];

  for (const instruction of instructions) {
    for (let i = 0; i < instruction.steps; i++) {
      switch (instruction.direction) {
        case Direction.UP:
          currentPoint.y--;
          break;
        case Direction.DOWN:
          currentPoint.y++;
          break;
        case Direction.LEFT:
          currentPoint.x--;
          break;
        case Direction.RIGHT:
          currentPoint.x++;
          break;
        default:
          exhaustiveCheck('Unhandled direction', instruction.direction);
      }
      loop.push({ ...currentPoint });
    }
  }

  return loop;
}

function capacity(loop: Loop): number {
  const loopSet = new Set(loop.map(pointToString));

  const minX = Math.min(...loop.map(point => point.x));
  const maxX = Math.max(...loop.map(point => point.x));
  const minY = Math.min(...loop.map(point => point.y));
  const maxY = Math.max(...loop.map(point => point.y));

  let capacity = 0;

  for (let y = minY; y <= maxY; y++) {
    for (let x = minX; x <= maxX; x++) {
      if (loopSet.has(pointToString({ x, y })) || pointInPolygon({ x, y }, loop)) {
        capacity++;
      }
    }
  }

  return capacity;
}

const instructions = parseInstructions(lines);

const loop = digLoop(instructions);

console.log(capacity(loop));

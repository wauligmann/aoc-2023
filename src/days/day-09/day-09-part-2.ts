import { readLines } from '../../utils/file';
import { sum } from '../../utils/math';

const lines = await readLines('day-09', 'input');

type History = number[];

type Report = History[];

function parseReport(lines: string[]): Report {
  return lines.map(line => line.split(' ').map(Number));
}

function prediction(history: History): number {
  let input = history;
  let sequences = [history];
  while (!input.every(value => value === 0)) {
    input = input.reduce((acc, value, index) => {
      if (index === 0) return acc;
      return [...acc, value - input[index - 1]];
    }, [] as History);
    sequences.push(input);
  }
  while (sequences.length >= 2) {
    const sequence = sequences.pop()!;
    const prevSequence = sequences.pop()!;
    sequences.push([prevSequence[0] - sequence[0], ...prevSequence]);
  }
  const finalSequence = sequences.pop()!;
  return finalSequence[0];
}

const report = parseReport(lines);

console.log(sum(report.map(prediction)));

import { readLines } from '../../utils/file';
import { isDigit, sum } from '../../utils/math';

const lines = await readLines('day-03', 'input');

type Schematic = {
  rows: number;
  columns: number;
  grid: string[][];
  numbers: Number[];
};

type Number = {
  value: number;
  row: number;
  startColumn: number;
  endColumn: number;
};

function parseLines(lines: string[]): Schematic {
  return {
    rows: lines.length,
    columns: lines[0].length,
    grid: lines.map(line => line.split('')),
    numbers: lines.flatMap((line, lineIndex) => {
      const numbers: Number[] = [];
      const chars = line.split('');

      let currentNumber = '';

      chars.forEach((char, index) => {
        if (isDigit(char)) currentNumber += char;
        if (!currentNumber) return;
        if (!isDigit(char) || index === chars.length - 1) {
          numbers.push({
            value: Number(currentNumber),
            row: lineIndex,
            startColumn: index - currentNumber.length,
            endColumn: index - 1,
          });
          currentNumber = '';
        }
      });

      return numbers;
    }),
  };
}

function isPartNumber(number: Number, schematic: Schematic): boolean {
  for (let row = number.row - 1; row <= number.row + 1; row++) {
    for (let column = number.startColumn - 1; column <= number.endColumn + 1; column++) {
      if (row < 0 || row >= schematic.rows || column < 0 || column >= schematic.columns) {
        continue;
      }
      if (!['.', ...'0123456789'].includes(schematic.grid[row][column])) {
        return true;
      }
    }
  }
  return false;
}

const schematic = parseLines(lines);

const partNumbers = schematic.numbers.filter(number => isPartNumber(number, schematic));

console.log(sum(partNumbers.map(number => number.value)));

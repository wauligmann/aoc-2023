import { readLines } from '../../utils/file';
import { inRange, isDigit, product, sum } from '../../utils/math';

const lines = await readLines('day-03', 'input');

type Schematic = {
  rows: number;
  columns: number;
  grid: string[][];
  numbers: Number[];
};

type Number = {
  value: number;
  row: number;
  startColumn: number;
  endColumn: number;
};

type Gear = {
  row: number;
  column: number;
  numbers: Number[]; // exactly 2
};

function parseLines(lines: string[]): Schematic {
  return {
    rows: lines.length,
    columns: lines[0].length,
    grid: lines.map(line => line.split('')),
    numbers: lines.flatMap((line, lineIndex) => {
      const numbers: Number[] = [];
      const chars = line.split('');

      let currentNumber = '';

      chars.forEach((char, index) => {
        if (isDigit(char)) currentNumber += char;
        if (!currentNumber) return;
        if (!isDigit(char) || index === chars.length - 1) {
          numbers.push({
            value: Number(currentNumber),
            row: lineIndex,
            startColumn: index - currentNumber.length,
            endColumn: index - 1,
          });
          currentNumber = '';
        }
      });

      return numbers;
    }),
  };
}

function isPartNumber(number: Number, schematic: Schematic): boolean {
  for (let row = number.row - 1; row <= number.row + 1; row++) {
    for (let column = number.startColumn - 1; column <= number.endColumn + 1; column++) {
      if (row < 0 || row >= schematic.rows || column < 0 || column >= schematic.columns) {
        continue;
      }
      if (!['.', ...'0123456789'].includes(schematic.grid[row][column])) {
        return true;
      }
    }
  }
  return false;
}

function adjacentPartNumbers(row: number, column: number, partNumbers: Number[]): Number[] {
  return partNumbers.filter(
    number =>
      inRange(number.row, { min: row - 1, max: row + 1 }) &&
      inRange(column, { min: number.startColumn - 1, max: number.endColumn + 1 }),
  );
}

function findGears(schematic: Schematic, partNumbers: Number[]): Gear[] {
  const gears: Gear[] = [];

  for (let row = 0; row < schematic.rows; row++) {
    for (let column = 0; column < schematic.columns; column++) {
      if (schematic.grid[row][column] !== '*') continue;

      const adjacentNumbers = adjacentPartNumbers(row, column, partNumbers);

      if (adjacentNumbers.length === 2) {
        gears.push({
          row,
          column,
          numbers: adjacentNumbers,
        });
      }
    }
  }

  return gears;
}

const schematic = parseLines(lines);

const partNumbers = schematic.numbers.filter(number => isPartNumber(number, schematic));

const gears = findGears(schematic, partNumbers);

console.log(sum(gears.map(gear => product(gear.numbers.map(number => number.value)))));

import { readLines } from '../../utils/file';
import { lcm } from '../../utils/math';
import { exhaustiveCheck } from '../../utils/typescript';

const lines = await readLines('day-20', 'input');

enum ModuleType {
  FLIP_FLOP = '%',
  CONJUNCTION = '&',
  BROADCAST = 'broadcaster',
  BUTTON = 'button',
}

type Node = string;

type Module =
  | {
      type: ModuleType.FLIP_FLOP;
      name: Node;
      on: boolean;
      destinations: Node[];
    }
  | {
      type: ModuleType.CONJUNCTION;
      name: Node;
      memory: Map<Node, PulseType>;
      destinations: Node[];
    }
  | {
      type: ModuleType.BROADCAST;
      name: ModuleType.BROADCAST;
      destinations: Node[];
    }
  | {
      type: ModuleType.BUTTON;
      name: ModuleType.BUTTON;
      destinations: Node[];
    };

enum PulseType {
  LOW = 'low',
  HIGH = 'high',
}

type Pulse = {
  type: PulseType;
  src: Node;
  dest: Node;
};

const TARGET_NODE: Node = 'rx';
const TARGET_PULSE_TYPE: PulseType = PulseType.LOW;

function parseModules(lines: string[]): Module[] {
  function buildModule(ident: string, destinations: Node[]): Module {
    switch (true) {
      case ident.startsWith(ModuleType.FLIP_FLOP):
        return { type: ModuleType.FLIP_FLOP, name: ident.slice(1), on: false, destinations };
      case ident.startsWith(ModuleType.CONJUNCTION):
        return {
          type: ModuleType.CONJUNCTION,
          name: ident.slice(1),
          memory: new Map(),
          destinations,
        };
      case ident === ModuleType.BROADCAST:
        return { type: ModuleType.BROADCAST, name: ModuleType.BROADCAST, destinations };
      default:
        throw new Error(`Unexpected ident: ${ident}`);
    }
  }

  const modules = lines.map(line => {
    const [ident, destinations] = line.split(' -> ');
    return buildModule(ident, destinations.split(', '));
  });

  modules.forEach(module => {
    if (module.type === ModuleType.CONJUNCTION) {
      modules
        .filter(m => m.destinations.includes(module.name))
        .forEach(input => {
          module.memory.set(input.name, PulseType.LOW);
        });
    }
  });

  return modules;
}

const BUTTON_MODULE: Module = {
  type: ModuleType.BUTTON,
  name: ModuleType.BUTTON,
  destinations: [ModuleType.BROADCAST],
};

const globalModules: Module[] = [BUTTON_MODULE, ...parseModules(lines)];

const INITIAL_PULSE: Pulse = {
  type: PulseType.LOW,
  src: ModuleType.BUTTON,
  dest: ModuleType.BUTTON,
};

function pushButton(modules: Module[]): Pulse[] {
  const history: Pulse[] = [];

  const queue: Pulse[] = [INITIAL_PULSE];

  while (queue.length > 0) {
    const pulse = queue.shift()!;

    history.push(pulse);

    const receiver = modules.find(m => m.name === pulse.dest);

    if (!receiver) {
      // This means the receiver has no destinations and we can skip it
      continue;
    }

    switch (receiver.type) {
      case ModuleType.BUTTON:
        queue.push({ type: PulseType.LOW, src: ModuleType.BUTTON, dest: ModuleType.BROADCAST });
        break;
      case ModuleType.BROADCAST:
        receiver.destinations.forEach(destination => {
          queue.push({ type: pulse.type, src: receiver.name, dest: destination });
        });
        break;
      case ModuleType.FLIP_FLOP:
        if (pulse.type === PulseType.LOW) {
          receiver.on = !receiver.on;
          receiver.destinations.forEach(destination => {
            queue.push({
              type: receiver.on ? PulseType.HIGH : PulseType.LOW,
              src: receiver.name,
              dest: destination,
            });
          });
        }
        break;
      case ModuleType.CONJUNCTION:
        receiver.memory.set(pulse.src, pulse.type);
        receiver.destinations.forEach(destination => {
          if ([...receiver.memory.values()].every(pulseType => pulseType === PulseType.HIGH)) {
            queue.push({ type: PulseType.LOW, src: receiver.name, dest: destination });
          } else {
            queue.push({ type: PulseType.HIGH, src: receiver.name, dest: destination });
          }
        });
        break;
      default:
        exhaustiveCheck('Unhandeled module type of receiver', receiver);
    }
  }

  return history.slice(1);
}

function minButtonPressesUntilTarget(target: Node, pulseType: PulseType): number {
  let step = 0;

  const modules = structuredClone(globalModules);

  while (true) {
    const pulses = pushButton(modules);
    step++;

    if (pulses.some(pulse => pulse.dest === target && pulse.type === pulseType)) {
      break;
    }
  }

  return step;
}

function prevTargets(...targets: Node[]): Node[] {
  return globalModules
    .filter(module => targets.some(target => module.destinations.includes(target)))
    .map(m => m.name);
}

const previousTargets: Node[] = prevTargets(...prevTargets(TARGET_NODE));

console.log(
  lcm(previousTargets.map(target => minButtonPressesUntilTarget(target, TARGET_PULSE_TYPE))),
);

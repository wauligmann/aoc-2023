import { partition } from '../../utils/array';
import { readLines } from '../../utils/file';
import { exhaustiveCheck } from '../../utils/typescript';

const lines = await readLines('day-20', 'input');

const STEPS = 1000;

enum ModuleType {
  FLIP_FLOP = '%',
  CONJUNCTION = '&',
  BROADCAST = 'broadcaster',
  BUTTON = 'button',
}

type Node = string;

type Module =
  | {
      type: ModuleType.FLIP_FLOP;
      name: Node;
      on: boolean;
      destinations: Node[];
    }
  | {
      type: ModuleType.CONJUNCTION;
      name: Node;
      memory: Map<Node, PulseType>;
      destinations: Node[];
    }
  | {
      type: ModuleType.BROADCAST;
      name: ModuleType.BROADCAST;
      destinations: Node[];
    }
  | {
      type: ModuleType.BUTTON;
      name: ModuleType.BUTTON;
      destinations: Node[];
    };

enum PulseType {
  LOW = 'low',
  HIGH = 'high',
}

type Pulse = {
  type: PulseType;
  src: Node;
  dest: Node;
};

function parseModules(lines: string[]): Module[] {
  function buildModule(ident: string, destinations: Node[]): Module {
    switch (true) {
      case ident.startsWith(ModuleType.FLIP_FLOP):
        return { type: ModuleType.FLIP_FLOP, name: ident.slice(1), on: false, destinations };
      case ident.startsWith(ModuleType.CONJUNCTION):
        return {
          type: ModuleType.CONJUNCTION,
          name: ident.slice(1),
          memory: new Map(),
          destinations,
        };
      case ident === ModuleType.BROADCAST:
        return { type: ModuleType.BROADCAST, name: ModuleType.BROADCAST, destinations };
      default:
        throw new Error(`Unexpected ident: ${ident}`);
    }
  }

  const modules = lines.map(line => {
    const [ident, destinations] = line.split(' -> ');
    return buildModule(ident, destinations.split(', '));
  });

  modules.forEach(module => {
    if (module.type === ModuleType.CONJUNCTION) {
      modules
        .filter(m => m.destinations.includes(module.name))
        .forEach(input => {
          module.memory.set(input.name, PulseType.LOW);
        });
    }
  });

  return modules;
}

const BUTTON_MODULE: Module = {
  type: ModuleType.BUTTON,
  name: ModuleType.BUTTON,
  destinations: [ModuleType.BROADCAST],
};

const modules = [BUTTON_MODULE, ...parseModules(lines)];

const INITIAL_PULSE: Pulse = {
  type: PulseType.LOW,
  src: ModuleType.BUTTON,
  dest: ModuleType.BUTTON,
};

function pushButton(): Pulse[] {
  const history: Pulse[] = [];

  const queue: Pulse[] = [INITIAL_PULSE];

  while (queue.length > 0) {
    const pulse = queue.shift()!;

    history.push(pulse);

    const receiver = modules.find(m => m.name === pulse.dest);

    if (!receiver) {
      // This means the receiver has no destinations and we can skip it
      continue;
    }

    switch (receiver.type) {
      case ModuleType.BUTTON:
        queue.push({ type: PulseType.LOW, src: ModuleType.BUTTON, dest: ModuleType.BROADCAST });
        break;
      case ModuleType.BROADCAST:
        receiver.destinations.forEach(destination => {
          queue.push({ type: pulse.type, src: receiver.name, dest: destination });
        });
        break;
      case ModuleType.FLIP_FLOP:
        if (pulse.type === PulseType.LOW) {
          receiver.on = !receiver.on;
          receiver.destinations.forEach(destination => {
            queue.push({
              type: receiver.on ? PulseType.HIGH : PulseType.LOW,
              src: receiver.name,
              dest: destination,
            });
          });
        }
        break;
      case ModuleType.CONJUNCTION:
        receiver.memory.set(pulse.src, pulse.type);
        receiver.destinations.forEach(destination => {
          if ([...receiver.memory.values()].every(pulseType => pulseType === PulseType.HIGH)) {
            queue.push({ type: PulseType.LOW, src: receiver.name, dest: destination });
          } else {
            queue.push({ type: PulseType.HIGH, src: receiver.name, dest: destination });
          }
        });
        break;
      default:
        exhaustiveCheck('Unhandeled module type of receiver', receiver);
    }
  }

  return history.slice(1);
}

function hashModulesState(modules: Module[]): string {
  return modules
    .map(module => {
      switch (module.type) {
        case ModuleType.FLIP_FLOP:
          return module.on ? '1' : '0';
        case ModuleType.CONJUNCTION:
          return [...module.memory.values()]
            .map(pulseType => (pulseType === PulseType.HIGH ? '1' : '0'))
            .join('');
        default:
          return '';
      }
    })
    .join('');
}

function multiply(pulseHistory: Pulse[], factor = 1): number {
  const [lowPulses, highPulses] = partition(pulseHistory, pulse => {
    return pulse.type === PulseType.LOW;
  });
  return lowPulses.length * factor * (highPulses.length * factor);
}

function totalMultiply(): number {
  const seen = new Map<string, number>();
  seen.set(hashModulesState(modules), 0);

  let step = 0;
  const pulseHistory: Pulse[] = [];

  while (step < STEPS) {
    pulseHistory.push(...pushButton());
    step++;
    if (seen.has(hashModulesState(modules))) break;
    seen.set(hashModulesState(modules), step);
  }

  const remainder = STEPS % step;

  let totalMultiply = multiply(pulseHistory, (STEPS - remainder) / step);

  for (let i = 0; i < remainder; i++) {
    totalMultiply += multiply(pushButton());
  }

  return totalMultiply;
}

console.log(totalMultiply());

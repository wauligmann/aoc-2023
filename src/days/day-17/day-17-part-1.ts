import { binaryInsert, last } from '../../utils/array';
import { readLines } from '../../utils/file';
import { exhaustiveCheck } from '../../utils/typescript';

const lines = await readLines('day-17', 'input');

const SAME_DIR_LIMIT = 3;

type Grid = number[][];

type Point = {
  y: number;
  x: number;
};

enum Direction {
  UP = '^',
  DOWN = 'v',
  LEFT = '<',
  RIGHT = '>',
}

type Visit = {
  point: Point;
  directions: Direction[];
  hash: string;
  cost: number;
  distance: number;
};

const allDirections = [Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT] as const;

function parseGrid(lines: string[]): Grid {
  return lines.map(line => line.split('').map(Number));
}

const grid = parseGrid(lines);

function valueAt(point: Point): number {
  return grid[point.y][point.x];
}

function hash(visit: Pick<Visit, 'point' | 'directions'>): string {
  return `${visit.point.y};${visit.point.x};${visit.directions.join(',')}`;
}

function isEqual(a: Point, b: Point): boolean {
  return a.y === b.y && a.x === b.x;
}

function move(point: Point, direction: Direction): Point {
  switch (direction) {
    case Direction.UP:
      return { x: point.x, y: point.y - 1 };
    case Direction.DOWN:
      return { x: point.x, y: point.y + 1 };
    case Direction.LEFT:
      return { x: point.x - 1, y: point.y };
    case Direction.RIGHT:
      return { x: point.x + 1, y: point.y };
  }
}

function inGrid(point: Point): boolean {
  return point.y >= 0 && point.y < grid.length && point.x >= 0 && point.x < grid[0].length;
}

function opposite(direction: Direction): Direction {
  switch (direction) {
    case Direction.UP:
      return Direction.DOWN;
    case Direction.DOWN:
      return Direction.UP;
    case Direction.LEFT:
      return Direction.RIGHT;
    case Direction.RIGHT:
      return Direction.LEFT;
    default:
      return exhaustiveCheck('Unhandeled direction', direction);
  }
}

function reachedStraightLimit(history: Direction[]): boolean {
  const recent = history.slice(-SAME_DIR_LIMIT);
  return recent.length === SAME_DIR_LIMIT && recent.every(direction => direction === recent[0]);
}

function getNeighbors(visit: Visit): Visit[] {
  const forbiddenDirections =
    visit.directions.length === 0
      ? []
      : [
          opposite(last(visit.directions)),
          ...(reachedStraightLimit(visit.directions) ? [last(visit.directions)] : []),
        ];

  return allDirections
    .filter(direction => !forbiddenDirections.includes(direction))
    .map(direction => ({
      point: move(visit.point, direction),
      directions: [...visit.directions, direction].slice(-SAME_DIR_LIMIT),
    }))
    .filter(pointAndDirection => inGrid(pointAndDirection.point))
    .map(pointAndDirection => ({
      ...pointAndDirection,
      cost: valueAt(pointAndDirection.point),
      hash: hash(pointAndDirection),
      distance: visit.distance + valueAt(pointAndDirection.point),
    }));
}

function shortestPathCost({ start, end }: { start: Point; end: Point }): number {
  const queue: Visit[] = [
    {
      point: start,
      directions: [],
      cost: 0,
      distance: 0,
    },
  ].map(visit => ({ ...visit, hash: hash(visit) }));

  const visited = new Set<string>();
  const previous = new Map<string, Visit[]>();

  while (queue.length > 0) {
    const visit = queue.shift()!;
    const visits = previous.get(visit.hash) ?? [];

    if (isEqual(visit.point, end)) {
      return visit.distance;
    }

    const neighbors = getNeighbors(visit);

    for (const neighbor of neighbors) {
      if (!visited.has(neighbor.hash)) {
        binaryInsert(queue, neighbor, (a, b) => a.distance - b.distance);
        previous.set(neighbor.hash, [...visits, neighbor]);
        visited.add(neighbor.hash);
      }
    }
  }

  throw new Error('No path found');
}

const cost = shortestPathCost({
  start: { y: 0, x: 0 },
  end: { y: grid.length - 1, x: grid[0].length - 1 },
});

console.log(cost);

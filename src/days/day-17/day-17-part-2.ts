import { binaryInsert, last, range, repeat } from '../../utils/array';
import { readLines } from '../../utils/file';
import { exhaustiveCheck } from '../../utils/typescript';

const lines = await readLines('day-17', 'input');

const SAME_DIR_MIN = 4;
const SAME_DIR_MAX = 10;

type Grid = number[][];

type Point = {
  y: number;
  x: number;
};

enum Direction {
  UP = '^',
  DOWN = 'v',
  LEFT = '<',
  RIGHT = '>',
}

type Visit = {
  point: Point;
  directions: Direction[];
  hash: string;
  distance: number;
};

const allDirections = [Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT] as const;

function parseGrid(lines: string[]): Grid {
  return lines.map(line => line.split('').map(Number));
}

const grid = parseGrid(lines);

function valueAt(point: Point): number {
  return grid[point.y][point.x];
}

function hash(visit: Pick<Visit, 'point' | 'directions'>): string {
  return `${visit.point.y};${visit.point.x};${visit.directions.join(',')}`;
}

function isEqual(a: Point, b: Point): boolean {
  return a.y === b.y && a.x === b.x;
}

function move(point: Point, direction: Direction, factor = 1): Point {
  switch (direction) {
    case Direction.UP:
      return { x: point.x, y: point.y - factor };
    case Direction.DOWN:
      return { x: point.x, y: point.y + factor };
    case Direction.LEFT:
      return { x: point.x - factor, y: point.y };
    case Direction.RIGHT:
      return { x: point.x + factor, y: point.y };
  }
}

function inGrid(point: Point): boolean {
  return point.y >= 0 && point.y < grid.length && point.x >= 0 && point.x < grid[0].length;
}

function opposite(direction: Direction): Direction {
  switch (direction) {
    case Direction.UP:
      return Direction.DOWN;
    case Direction.DOWN:
      return Direction.UP;
    case Direction.LEFT:
      return Direction.RIGHT;
    case Direction.RIGHT:
      return Direction.LEFT;
    default:
      return exhaustiveCheck('Unhandeled direction', direction);
  }
}

function tailEqual<T>(array: T[], n: number): boolean {
  const recent = array.slice(-n);
  return recent.length === n && recent.every(element => element === recent[0]);
}

function reachedStraightMin(history: Direction[]): boolean {
  return tailEqual(history, SAME_DIR_MIN);
}

function reachedStraightLimit(history: Direction[]): boolean {
  return tailEqual(history, SAME_DIR_MAX);
}

function getNeighbors(visit: Visit): Visit[] {
  const forbiddenDirections =
    visit.directions.length === 0
      ? []
      : [
          opposite(last(visit.directions)),
          ...(reachedStraightLimit(visit.directions) ? [last(visit.directions)] : []),
          ...(reachedStraightMin(visit.directions)
            ? []
            : allDirections.filter(direction => direction !== last(visit.directions))),
        ];

  const moveFactor = reachedStraightMin(visit.directions) ? 1 : SAME_DIR_MIN - 1;

  return allDirections
    .filter(direction => !forbiddenDirections.includes(direction))
    .map(direction => ({
      point: move(visit.point, direction, moveFactor),
      directions: [...visit.directions, ...repeat([direction], moveFactor)].slice(-SAME_DIR_MAX),
    }))
    .filter(pointAndDirection => inGrid(pointAndDirection.point))
    .map(pointAndDirection => ({
      ...pointAndDirection,
      hash: hash(pointAndDirection),
      distance:
        visit.distance +
        range(1, moveFactor)
          .map(i => valueAt(move(visit.point, last(pointAndDirection.directions), i)))
          .reduce((a, b) => a + b, 0),
    }));
}

function shortestPathCost({ start, end }: { start: Point; end: Point }): number {
  const queue: Visit[] = [
    {
      point: start,
      directions: [],
      distance: 0,
    },
  ].map(visit => ({ ...visit, hash: hash(visit) }));

  const visited = new Set<string>();
  const previous = new Map<string, Visit[]>();

  while (queue.length > 0) {
    const visit = queue.shift()!;
    const visits = previous.get(visit.hash) ?? [];

    if (isEqual(visit.point, end) && reachedStraightMin(visit.directions)) {
      return visit.distance;
    }

    const neighbors = getNeighbors(visit);

    for (const neighbor of neighbors) {
      if (!visited.has(neighbor.hash)) {
        binaryInsert(queue, neighbor, (a, b) => a.distance - b.distance);
        previous.set(neighbor.hash, [...visits, neighbor]);
        visited.add(neighbor.hash);
      }
    }
  }

  throw new Error('No path found');
}

const cost = shortestPathCost({
  start: { y: 0, x: 0 },
  end: { y: grid.length - 1, x: grid[0].length - 1 },
});

console.log(cost);

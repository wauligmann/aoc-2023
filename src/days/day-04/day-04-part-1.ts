import { readLines } from '../../utils/file';
import { sum } from '../../utils/math';

const lines = await readLines('day-04', 'input');

type Card = {
  id: number;
  winningNumbers: number[];
  myNumbers: Set<number>;
};

type Evaluation = {
  card: Card;
  points: number;
};

function parseCard(line: string): Card {
  const [prefix, tail] = line.split(': ');
  const [winningNumbers, myNumbers] = tail.split('|');
  const toNumbers = (str: string) => str.split(' ').filter(Boolean).map(Number);
  return {
    id: Number(prefix.slice('Card'.length)),
    winningNumbers: toNumbers(winningNumbers),
    myNumbers: new Set(toNumbers(myNumbers)),
  };
}

function evaluateCard(card: Card): Evaluation {
  const matches = card.winningNumbers.filter(number => card.myNumbers.has(number)).length;
  return { card, points: matches > 0 ? 2 ** (matches - 1) : 0 };
}

const cards = lines.map(parseCard);

const evaluations = cards.map(evaluateCard);

console.log(sum(evaluations.map(({ points }) => points)));

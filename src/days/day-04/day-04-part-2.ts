import { range } from '../../utils/array';
import { readLines } from '../../utils/file';
import { sum } from '../../utils/math';

const lines = await readLines('day-04', 'input');

type Card = {
  row: number;
  matches: number;
};

function parseCard(line: string): Card {
  const [prefix, tail] = line.split(': ');
  const [winningNumbers, myNumbers] = tail.split('|');
  const toNumbers = (str: string) => str.split(' ').filter(Boolean).map(Number);
  const myNumbersSet = new Set(toNumbers(myNumbers));
  return {
    row: Number(prefix.slice('Card'.length)) - 1,
    matches: toNumbers(winningNumbers).filter(n => myNumbersSet.has(n)).length,
  };
}

const cards = lines.map(parseCard);
const cache = new Map<Card['row'], number>();

function numberOfCards(card: Card): number {
  return (
    cache.get(card.row) ||
    cache
      .set(card.row, 1 + sum(range(1, card.matches).map(dy => numberOfCards(cards[card.row + dy]))))
      .get(card.row)!
  );
}

console.log(sum(cards.map(numberOfCards)));

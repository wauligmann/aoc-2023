import { repeat } from '../../utils/array';
import { readLines } from '../../utils/file';
import { sum } from '../../utils/math';

const lines = await readLines('day-12', 'input');

const REPEAT_COUNT = 5;

enum State {
  Operational = '.',
  Damaged = '#',
  Unknown = '?',
}

type Row = {
  states: State[];
  damagedGroupCounts: number[];
};

function parseRow(line: string): Row {
  const [states, counts] = line.split(' ');
  return {
    states: states.split('') as State[],
    damagedGroupCounts: counts.split(',').map(Number),
  };
}

function unfoldRow(row: Row): Row {
  return {
    states: repeat(row.states, REPEAT_COUNT, State.Unknown),
    damagedGroupCounts: repeat(row.damagedGroupCounts, REPEAT_COUNT),
  };
}

const cache = new Map<string, number>();

function possibleArrangementCount(row: Row): number {
  return arrangementCount(row.states, 0, row.damagedGroupCounts);
}

function arrangementCount(
  states: State[],
  currentDamagedGroupCount: number,
  remainingDamagedGroupCounts: number[],
): number {
  const cacheKey = [
    states.join(''),
    currentDamagedGroupCount,
    remainingDamagedGroupCounts.join(','),
  ].join('-');

  if (cache.has(cacheKey)) {
    return cache.get(cacheKey)!;
  }

  const count = computeArrangementCount(
    states,
    currentDamagedGroupCount,
    remainingDamagedGroupCounts,
  );

  cache.set(cacheKey, count);

  return count;
}

function computeArrangementCount(
  states: State[],
  currentDamagedGroupCount: number,
  remainingDamagedGroupCounts: number[],
): number {
  if (states.length === 0) {
    return remainingDamagedGroupCounts.length === 0 ||
      remainingDamagedGroupCounts[remainingDamagedGroupCounts.length - 1] === 0
      ? 1
      : 0;
  }

  if (states.length < sum(remainingDamagedGroupCounts) + remainingDamagedGroupCounts.length - 1) {
    return 0;
  }

  const [headCounts, ...tailCounts] = remainingDamagedGroupCounts;

  switch (states[0]) {
    case State.Operational:
      if (currentDamagedGroupCount > 0 && headCounts > 0) return 0;
      return arrangementCount(
        states.slice(1),
        0,
        headCounts === 0 ? tailCounts : remainingDamagedGroupCounts,
      );
    case State.Damaged:
      if (headCounts === 0) return 0;
      return arrangementCount(states.slice(1), currentDamagedGroupCount + 1, [
        headCounts - 1,
        ...tailCounts,
      ]);
    case State.Unknown:
      return (
        arrangementCount(
          [State.Operational, ...states.slice(1)],
          currentDamagedGroupCount,
          remainingDamagedGroupCounts,
        ) +
        arrangementCount(
          [State.Damaged, ...states.slice(1)],
          currentDamagedGroupCount,
          remainingDamagedGroupCounts,
        )
      );
  }
}

const unfoldedRows = lines.map(parseRow).map(unfoldRow);

console.log(sum(unfoldedRows.map(possibleArrangementCount)));

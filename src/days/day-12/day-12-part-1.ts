import { chunkBy, isEqual } from '../../utils/array';
import { readLines } from '../../utils/file';
import { sum } from '../../utils/math';

const lines = await readLines('day-12', 'input');

enum State {
  Operational = '.',
  Damaged = '#',
  Unknown = '?',
}

type KnownState = State.Operational | State.Damaged;

type Row = {
  states: State[];
  damagedGroupCounts: number[];
};

function parseRow(line: string): Row {
  const [states, counts] = line.split(' ');
  return {
    states: states.split('') as State[],
    damagedGroupCounts: counts.split(',').map(Number),
  };
}

function possibleArrangements(row: Row): KnownState[][] {
  let possibleStates: KnownState[][] = [[]];

  row.states.forEach((state, index) => {
    if (state === State.Unknown) {
      possibleStates = possibleStates.flatMap(arrangement => {
        return [
          ...(possiblePrefix([...arrangement, State.Operational], row.damagedGroupCounts)
            ? [[...arrangement, State.Operational as KnownState]]
            : []),
          ...(possiblePrefix([...arrangement, State.Damaged], row.damagedGroupCounts)
            ? [[...arrangement, State.Damaged as KnownState]]
            : []),
        ];
      });
    } else {
      possibleStates = possibleStates.flatMap(arrangement => {
        return possiblePrefix([...arrangement, state], row.damagedGroupCounts)
          ? [[...arrangement, state]]
          : [];
      });
    }
  });

  possibleStates = possibleStates.filter(arrangement =>
    isPossibleArrangement(arrangement, row.damagedGroupCounts),
  );

  return possibleStates;
}

function possiblePrefix(
  states: KnownState[],
  damagedGroupCounts: Row['damagedGroupCounts'],
): boolean {
  let currentState: KnownState = State.Operational;
  let currentDamageCount = 0;
  let currentDamageCountGroupIndex = 0;

  for (let index = 0; index < states.length; index++) {
    const newState = states[index];

    if (currentState === State.Operational) {
      if (newState === State.Damaged) {
        currentDamageCount = 1;
      }
    } else {
      if (newState === State.Damaged) {
        currentDamageCount++;

        if (currentDamageCount > damagedGroupCounts[currentDamageCountGroupIndex]) {
          return false;
        }
      } else {
        if (currentDamageCount !== damagedGroupCounts[currentDamageCountGroupIndex]) {
          return false;
        }

        currentDamageCountGroupIndex++;
      }
    }

    currentState = newState;
  }

  return true;
}

function isPossibleArrangement(
  states: KnownState[],
  damagedGroupCounts: Row['damagedGroupCounts'],
): boolean {
  const isOperational = (state: KnownState) => state === State.Operational;
  const chunks = chunkBy(states, isOperational).filter(chunk => chunk.length > 0);
  return isEqual(
    chunks.map(chunk => chunk.length),
    damagedGroupCounts,
  );
}

const rows = lines.map(parseRow);

console.log(sum(rows.map(possibleArrangements).map(arrangements => arrangements.length)));

import { transpose } from '../../utils/array';
import { readLines } from '../../utils/file';

const lines = await readLines('day-14', 'input');

const CYCLES = 1_000_000_000;

enum Tile {
  RoundedRock = 'O',
  CubeShapedRock = '#',
  EmptySpace = '.',
}

type Platform = Tile[][];

function parsePlatform(lines: string[]): Platform {
  return lines.map(line => line.split('') as Tile[]);
}

function platformToString(platform: Platform): string {
  return platform.map(line => line.join('')).join('\n');
}

function tiltNorth(platform: Platform): Platform {
  const columns = transpose(platform);

  columns.forEach(column => {
    column.forEach((tile, index) => {
      if (tile === Tile.RoundedRock) {
        let lowestPosition = index;
        while (lowestPosition > 0 && column[lowestPosition - 1] === Tile.EmptySpace) {
          lowestPosition--;
        }
        if (lowestPosition !== index) {
          column[lowestPosition] = Tile.RoundedRock;
          column[index] = Tile.EmptySpace;
        }
      }
    });
  });

  return transpose(columns);
}

function tiltSouth(platform: Platform): Platform {
  const columns = transpose(platform);

  columns.forEach(column => {
    for (let index = column.length - 1; index >= 0; index--) {
      const tile = column[index];
      if (tile === Tile.RoundedRock) {
        let highestPosition = index;
        while (
          highestPosition < column.length - 1 &&
          column[highestPosition + 1] === Tile.EmptySpace
        ) {
          highestPosition++;
        }
        if (highestPosition !== index) {
          column[highestPosition] = Tile.RoundedRock;
          column[index] = Tile.EmptySpace;
        }
      }
    }
  });

  return transpose(columns);
}

function tiltWest(platform: Platform): Platform {
  const rows = platform.map(line => [...line]);

  rows.forEach(row => {
    row.forEach((tile, index) => {
      if (tile === Tile.RoundedRock) {
        let lowestPosition = index;
        while (lowestPosition > 0 && row[lowestPosition - 1] === Tile.EmptySpace) {
          lowestPosition--;
        }
        if (lowestPosition !== index) {
          row[lowestPosition] = Tile.RoundedRock;
          row[index] = Tile.EmptySpace;
        }
      }
    });
  });

  return rows;
}

function tiltEast(platform: Platform): Platform {
  const rows = platform.map(line => [...line]);

  rows.forEach(row => {
    for (let index = row.length - 1; index >= 0; index--) {
      const tile = row[index];
      if (tile === Tile.RoundedRock) {
        let highestPosition = index;
        while (highestPosition < row.length - 1 && row[highestPosition + 1] === Tile.EmptySpace) {
          highestPosition++;
        }
        if (highestPosition !== index) {
          row[highestPosition] = Tile.RoundedRock;
          row[index] = Tile.EmptySpace;
        }
      }
    }
  });

  return rows;
}

function performCycle(platform: Platform): Platform {
  return tiltEast(tiltSouth(tiltWest(tiltNorth(platform))));
}

function performCycles(platform: Platform, cycles: number): Platform {
  const seenMap = new Map<string, number>();
  seenMap.set(platformToString(platform), 0);

  for (let index = 1; index <= cycles; index++) {
    platform = performCycle(platform);

    const previousCycle = seenMap.get(platformToString(platform));

    if (previousCycle) {
      const cycleLength = index - previousCycle;
      const remainingCycles = (cycles - index) % cycleLength;

      for (let i = 0; i < remainingCycles; i++) {
        platform = performCycle(platform);
      }

      return platform;
    }

    seenMap.set(platformToString(platform), index);
  }

  return platform;
}

function calculateLoad(platform: Platform): number {
  return platform.reduce((acc, line, lineIndex) => {
    return (
      acc +
      line.reduce((acc, tile) => {
        if (tile === Tile.RoundedRock) {
          return acc + platform.length - lineIndex;
        }
        return acc;
      }, 0)
    );
  }, 0);
}

const platform = parsePlatform(lines);

const cycledPlatform = performCycles(platform, CYCLES);

console.log(calculateLoad(cycledPlatform));

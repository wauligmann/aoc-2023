import { transpose } from '../../utils/array';
import { readLines } from '../../utils/file';

const lines = await readLines('day-14', 'input');

enum Tile {
  RoundedRock = 'O',
  CubeShapedRock = '#',
  EmptySpace = '.',
}

type Platform = Tile[][];

function parsePlatform(lines: string[]): Platform {
  return lines.map(line => line.split('') as Tile[]);
}

function tiltNorth(platform: Platform): Platform {
  const columns = transpose(platform);

  columns.forEach(column => {
    column.forEach((tile, index) => {
      if (tile === Tile.RoundedRock) {
        let lowestPosition = index;
        while (lowestPosition > 0 && column[lowestPosition - 1] === Tile.EmptySpace) {
          lowestPosition--;
        }
        if (lowestPosition !== index) {
          column[lowestPosition] = Tile.RoundedRock;
          column[index] = Tile.EmptySpace;
        }
      }
    });
  });

  return transpose(columns);
}

function calculateLoad(platform: Platform): number {
  return platform.reduce((acc, line, lineIndex) => {
    return (
      acc +
      line.reduce((acc, tile) => {
        if (tile === Tile.RoundedRock) {
          return acc + platform.length - lineIndex;
        }
        return acc;
      }, 0)
    );
  }, 0);
}

const platform = parsePlatform(lines);

const tiltedPlatform = tiltNorth(platform);

console.log(calculateLoad(tiltedPlatform));

import { readLines } from '../../utils/file';
import { longestPath } from '../../utils/graph';
import { exhaustiveCheck } from '../../utils/typescript';

const lines = await readLines('day-23', 'input');

enum Tile {
  PATH = '.',
  FOREST = '#',
  SLOPE_UP = '^',
  SLOPE_DOWN = 'v',
  SLOPE_LEFT = '<',
  SLOPE_RIGHT = '>',
}

type Slope = Tile.SLOPE_UP | Tile.SLOPE_DOWN | Tile.SLOPE_LEFT | Tile.SLOPE_RIGHT;

type Grid = Tile[][];

type Point = {
  y: number;
  x: number;
};

function parseGrid(lines: string[]): Grid {
  return lines.map(line => line.split('') as Tile[]);
}

function tileAt(point: Point): Tile {
  return grid[point.y]?.[point.x] ?? Tile.FOREST;
}

function isSlope(tile: Tile): tile is Slope {
  return (
    tile === Tile.SLOPE_UP ||
    tile === Tile.SLOPE_DOWN ||
    tile === Tile.SLOPE_LEFT ||
    tile === Tile.SLOPE_RIGHT
  );
}

function isWalkable(point: Point): boolean {
  return tileAt(point) !== Tile.FOREST;
}

function findPathPoint(y: number): Point {
  return { y, x: grid[y].findIndex(t => t === Tile.PATH) };
}

function neighbors(point: Point): Point[] {
  const tile = tileAt(point);

  if (isSlope(tile)) {
    switch (tile) {
      case Tile.SLOPE_UP:
        return [{ y: point.y - 1, x: point.x }];
      case Tile.SLOPE_DOWN:
        return [{ y: point.y + 1, x: point.x }];
      case Tile.SLOPE_LEFT:
        return [{ y: point.y, x: point.x - 1 }];
      case Tile.SLOPE_RIGHT:
        return [{ y: point.y, x: point.x + 1 }];
      default:
        exhaustiveCheck('Unhandled slope', tile);
    }
  }

  return [
    { y: point.y - 1, x: point.x },
    { y: point.y + 1, x: point.x },
    { y: point.y, x: point.x - 1 },
    { y: point.y, x: point.x + 1 },
  ].filter(to => canWalk(point, to));
}

function canWalk(from: Point, to: Point) {
  if (tileAt(from) !== Tile.PATH) {
    throw new Error('Function only works for paths');
  }

  const tileToEnter = tileAt(to);

  switch (tileToEnter) {
    case Tile.FOREST:
      return false;
    case Tile.PATH:
      return true;
    case Tile.SLOPE_UP:
      return from.y > to.y;
    case Tile.SLOPE_DOWN:
      return from.y < to.y;
    case Tile.SLOPE_LEFT:
      return from.x > to.x;
    case Tile.SLOPE_RIGHT:
      return from.x < to.x;
    default:
      exhaustiveCheck('Unhandled tile', tileToEnter);
  }
}

const grid = parseGrid(lines);

const start = findPathPoint(0);
const target = findPathPoint(grid.length - 1);

console.log(longestPath(grid, isWalkable, neighbors, start, target));

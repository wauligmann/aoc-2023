import { readLines } from '../../utils/file';
import { product, sum } from '../../utils/math';

const lines = await readLines('day-02', 'input');

type Game = {
  id: number;
  rounds: Round[];
};

const colors = ['red', 'green', 'blue'] as const;

type Color = (typeof colors)[number];

type Round = {
  [key in Color]: number;
};

type Bag = {
  [key in Color]: number;
};

function parseGame(line: string): Game {
  const [prefix, rawRounds] = line.split(': ');
  return {
    id: Number(prefix.slice('Game'.length + 1)),
    rounds: rawRounds.split('; ').map(rawRound => {
      return rawRound.split(', ').reduce(
        (round: Round, quantityAndColor) => {
          const [quantity, color] = quantityAndColor.split(' ');
          round[color as Color] = Number(quantity);
          return round;
        },
        {
          red: 0,
          green: 0,
          blue: 0,
        },
      );
    }),
  };
}

function minBag(game: Game): Bag {
  return game.rounds.reduce(
    (bag: Bag, round) => {
      colors.forEach(color => {
        bag[color] = Math.max(bag[color], round[color]);
      });
      return bag;
    },
    {
      red: 0,
      green: 0,
      blue: 0,
    },
  );
}

const minBags = lines.map(parseGame).map(minBag);

console.log(sum(minBags.map(bag => product(Object.values(bag)))));

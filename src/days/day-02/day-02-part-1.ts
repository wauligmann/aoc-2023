import { readLines } from '../../utils/file';
import { sum } from '../../utils/math';

const lines = await readLines('day-02', 'input');

type Game = {
  id: number;
  rounds: Round[];
};

const colors = ['red', 'green', 'blue'] as const;

type Color = (typeof colors)[number];

type Round = {
  [key in Color]: number;
};

type Bag = {
  [key in Color]: number;
};

const bag: Bag = {
  red: 12,
  green: 13,
  blue: 14,
};

function parseGame(line: string): Game {
  const [prefix, rawRounds] = line.split(': ');
  return {
    id: Number(prefix.slice('Game'.length + 1)),
    rounds: rawRounds.split('; ').map(rawRound => {
      return rawRound.split(', ').reduce(
        (round: Round, quantityAndColor) => {
          const [quantity, color] = quantityAndColor.split(' ');
          round[color as Color] = Number(quantity);
          return round;
        },
        {
          red: 0,
          green: 0,
          blue: 0,
        },
      );
    }),
  };
}

function isPossible(game: Game, bag: Bag): boolean {
  return game.rounds.every(round => {
    return colors.every(color => round[color] <= bag[color]);
  });
}

const possibleGames = lines.map(parseGame).filter(game => isPossible(game, bag));

console.log(sum(possibleGames.map(game => game.id)));

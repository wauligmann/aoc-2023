import { firstPresent, isOneOf, isPresent, partition, range } from '../../utils/array';
import { readLines } from '../../utils/file';
import { pointInPolygon } from '../../utils/geo';

const lines = await readLines('day-10', 'input');

const animal = 'S';
const pipes = ['|', '-', 'L', 'J', '7', 'F'] as const;
const ground = '.';

type Animal = typeof animal;
type Pipe = (typeof pipes)[number];
type Ground = typeof ground;

type Tile = Animal | Pipe | Ground;

type Grid = Tile[][];

type Coord = {
  x: number;
  y: number;
};

type Polygon = Coord[];

type Direction = 0 | 1;

const directions: Record<Pipe, [Coord, Coord]> = {
  '|': [
    { x: 0, y: -1 },
    { x: 0, y: 1 },
  ],
  '-': [
    { x: -1, y: 0 },
    { x: 1, y: 0 },
  ],
  L: [
    { x: 0, y: -1 },
    { x: 1, y: 0 },
  ],
  J: [
    { x: 0, y: -1 },
    { x: -1, y: 0 },
  ],
  '7': [
    { x: -1, y: 0 },
    { x: 0, y: 1 },
  ],
  F: [
    { x: 1, y: 0 },
    { x: 0, y: 1 },
  ],
};

function parseGrid(lines: string[]): Grid {
  return lines.map(line => line.split('') as Tile[]);
}

const grid = parseGrid(lines);

const startCoord: Coord = firstPresent(
  grid.map((row, y) => {
    const x = row.indexOf(animal);
    return x !== -1 ? { x, y } : null;
  }),
)!;

function isEqual(a: Coord, b: Coord): boolean {
  return a.x === b.x && a.y === b.y;
}

function walk(coord: Coord, delta: Coord): Coord {
  return { x: coord.x + delta.x, y: coord.y + delta.y };
}

function getLoop(startPipe: Pipe): Polygon | null {
  let polygon: Polygon = [];

  let currentCoord = startCoord;
  let currentPipe = startPipe;
  let currentDirection: Direction = 0;

  while (true) {
    const direction = directions[currentPipe][currentDirection];
    const nextCoord = walk(currentCoord, direction);
    const nextTile = grid[nextCoord.y]?.[nextCoord.x];

    polygon.push(currentCoord);

    if (nextTile === animal) return polygon;
    if (!isOneOf(nextTile, pipes)) return null;

    const [[usedDirection], [otherDirection]] = partition(directions[nextTile], direction =>
      isEqual(walk(nextCoord, direction), currentCoord),
    );
    if (!usedDirection) return null;

    currentDirection = directions[nextTile].indexOf(otherDirection) as Direction;
    currentPipe = nextTile;
    currentCoord = nextCoord;
  }
}

const loop = pipes.map(getLoop).find(isPresent)!;

const coordsInLoop = range(0, grid.length - 1)
  .flatMap<Coord>(y => range(0, grid[y].length - 1).map(x => ({ x, y })))
  .filter(coord => !loop.some(loopCoord => isEqual(coord, loopCoord)))
  .filter(coord => pointInPolygon(coord, loop));

console.log(coordsInLoop.length);

import { readLines } from '../../utils/file';
import { findIntersection } from '../../utils/geo';
import { inRange, pairs } from '../../utils/math';

const lines = await readLines('day-24', 'input');

const MIN_BOUND = 200_000_000_000_000;
const MAX_BOUND = 400_000_000_000_000;

type Position = {
  x: number;
  y: number;
  z: number;
};

type Velocity = {
  dx: number;
  dy: number;
  dz: number;
};

type Hailstone = {
  position: Position;
  velocity: Velocity;
};

function parseHailstones(lines: string[]): Hailstone[] {
  return lines.map(line => {
    const [position, velocity] = line.split(' @ ');
    const [x, y, z] = position.split(', ').map(Number);
    const [dx, dy, dz] = velocity.split(', ').map(Number);
    return {
      position: { x, y, z },
      velocity: { dx, dy, dz },
    };
  });
}

function toRay(hailstone: Hailstone) {
  return {
    position: hailstone.position,
    direction: hailstone.velocity,
  };
}

function intersectWithinBounds([a, b]: [Hailstone, Hailstone]): boolean {
  const intersection = findIntersection(toRay(a), toRay(b));
  return (
    intersection !== null &&
    inRange(intersection.x, { min: MIN_BOUND, max: MAX_BOUND }) &&
    inRange(intersection.y, { min: MIN_BOUND, max: MAX_BOUND })
  );
}

const hailstones = parseHailstones(lines);

const hailstonePairs = pairs(hailstones);

const intersectingPairs = hailstonePairs.filter(intersectWithinBounds);

console.log(intersectingPairs.length);

import { range } from '../../utils/array';
import { readLines } from '../../utils/file';
import { pairs, sum } from '../../utils/math';

const lines = await readLines('day-11', 'input');

const EXPAND_FACTOR = 1_000_000;

const empty = '.';
const galaxy = '#';

type Empty = typeof empty;
type Galaxy = typeof galaxy;
type Cell = Empty | Galaxy;

type Grid = Cell[][];

type Coord = {
  x: number;
  y: number;
};

function parseGrid(lines: string[]): Grid {
  return lines.map(line => line.split('') as Cell[]);
}

const grid = parseGrid(lines);

const rowsToExpand = grid.flatMap((row, y) => {
  return row.every(cell => cell === empty) ? [y] : [];
});

const colsToExpand = grid[0].flatMap((_, x) => {
  return grid.every(row => row[x] === empty) ? [x] : [];
});

function expandedDistance(a: Coord, b: Coord): number {
  const distance = (axis: keyof Coord) => {
    const expandedIndices = axis === 'x' ? colsToExpand : rowsToExpand;
    const [min, max] = [a, b].sort((a, b) => a[axis] - b[axis]);
    return sum(
      range(min[axis], max[axis] - 1).map(index => {
        return expandedIndices.includes(index) ? EXPAND_FACTOR : 1;
      }),
    );
  };
  return distance('x') + distance('y');
}

const galaxies: Coord[] = grid.flatMap((row, y) => {
  return row.flatMap((cell, x) => (cell == galaxy ? [{ x, y }] : []));
});

const galaxyPairs = pairs(galaxies);

console.log(sum(galaxyPairs.map(([a, b]) => expandedDistance(a, b))));

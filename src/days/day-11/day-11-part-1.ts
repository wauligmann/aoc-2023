import { transpose } from '../../utils/array';
import { readLines } from '../../utils/file';
import { manhattanDistance } from '../../utils/geo';
import { pairs, sum } from '../../utils/math';

const lines = await readLines('day-11', 'input');

const empty = '.';
const galaxy = '#';

type Empty = typeof empty;
type Galaxy = typeof galaxy;
type Cell = Empty | Galaxy;

type Grid = Cell[][];

type Coord = {
  x: number;
  y: number;
};

function parseGrid(lines: string[]): Grid {
  return lines.map(line => line.split('') as Cell[]);
}

function expandUniverse(universe: Grid): Grid {
  const expand = (grid: Grid): Grid => {
    return grid.flatMap((row, y) => {
      return row.every(cell => cell === empty) ? [row, row] : [row];
    });
  };
  return transpose(expand(transpose(expand(universe))));
}

const universe = parseGrid(lines);

const expandedUniverse = expandUniverse(universe);

const galaxies: Coord[] = expandedUniverse.flatMap((row, y) => {
  return row.flatMap((cell, x) => (cell == galaxy ? [{ x, y }] : []));
});

const galaxyPairs = pairs(galaxies);

console.log(sum(galaxyPairs.map(([a, b]) => manhattanDistance(a, b))));

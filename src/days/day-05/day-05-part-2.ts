import { chunkBy, chunksOf } from '../../utils/array';
import { readLines } from '../../utils/file';
import { sum } from '../../utils/math';

const lines = await readLines('day-05', 'input');

type Almanac = {
  seedRanges: Range[];
  maps: Map[];
};

type Range = {
  start: number;
  rangeLength: number;
};

type Map = Mapping[];

type Mapping = {
  sourceRangeStart: number;
  rangeLength: number;
  transformDelta: number;
};

function parseAlmanac(lines: string[]): Almanac {
  const [seeds, _, ...tail] = lines;
  return {
    seedRanges: chunksOf(seeds.slice('seeds: '.length).split(' ').map(Number), 2).map(
      ([start, rangeLength]) => ({
        start,
        rangeLength,
      }),
    ),
    maps: chunkBy(tail, line => !line).map(([_, ...mappings]) => {
      return mappings
        .map(mapping => {
          const [d, s, l] = mapping.split(' ').map(Number);
          return {
            sourceRangeStart: s,
            rangeLength: l,
            transformDelta: d - s,
          };
        })
        .toSorted((a, b) => {
          return a.sourceRangeStart - b.sourceRangeStart;
        });
    }),
  };
}

const almanac = parseAlmanac(lines);

function getLocationRanges(seedRange: Range): Range[] {
  return almanac.maps.reduce(
    (acc, map) => acc.flatMap(range => transformRange(range, map)),
    [seedRange],
  );
}

function transformRange(range: Range, map: Map): Range[] {
  const transformedRanges: Range[] = [];

  for (const mapping of map) {
    // mapping is entirely left of range
    if (mapping.sourceRangeStart + mapping.rangeLength <= range.start) {
      continue;
    }
    // mapping is entirely right of range
    if (mapping.sourceRangeStart >= range.start + range.rangeLength) {
      continue;
    }
    // mapping is left of range but overlaps
    if (mapping.sourceRangeStart < range.start) {
      const leftRange: Range = {
        start: range.start + mapping.transformDelta,
        rangeLength: Math.min(
          mapping.sourceRangeStart + mapping.rangeLength - range.start,
          range.rangeLength,
        ),
      };
      transformedRanges.push(leftRange);
      if (leftRange.rangeLength === range.rangeLength) return transformedRanges;
      range = {
        start: mapping.sourceRangeStart + mapping.rangeLength,
        rangeLength: range.rangeLength - leftRange.rangeLength,
      };
    }
    // mapping is right of range but overlaps
    else if (mapping.sourceRangeStart + mapping.rangeLength > range.start + range.rangeLength) {
      if (range.start < mapping.sourceRangeStart) {
        transformedRanges.push({
          start: range.start,
          rangeLength: mapping.sourceRangeStart - range.start,
        });
      }
      transformedRanges.push({
        start: mapping.sourceRangeStart + mapping.transformDelta,
        rangeLength: range.start + range.rangeLength - mapping.sourceRangeStart,
      });
      return transformedRanges;
    }
    // mapping is entirely inside range
    else {
      const ranges: Range[] = [];
      if (range.start < mapping.sourceRangeStart) {
        ranges.push({
          start: range.start,
          rangeLength: mapping.sourceRangeStart - range.start,
        });
      }
      ranges.push({
        start: mapping.sourceRangeStart + mapping.transformDelta,
        rangeLength: mapping.rangeLength,
      });
      transformedRanges.push(...ranges);
      if (sum(ranges.map(range => range.rangeLength)) === range.rangeLength)
        return transformedRanges;
      range = {
        start: mapping.sourceRangeStart + mapping.rangeLength,
        rangeLength:
          range.start + range.rangeLength - mapping.sourceRangeStart - mapping.rangeLength,
      };
    }
  }

  transformedRanges.push(range);

  return transformedRanges;
}

const locationRanges = almanac.seedRanges.flatMap(getLocationRanges);

console.log(Math.min(...locationRanges.map(range => range.start)));

import { chunkBy } from '../../utils/array';
import { readLines } from '../../utils/file';

const lines = await readLines('day-05', 'input');

type Almanac = {
  seeds: number[];
  maps: Map[];
};

type Map = Mapping[];

type Mapping = {
  destinationRangeStart: number;
  sourceRangeStart: number;
  rangeLength: number;
};

function parseAlmanac(lines: string[]): Almanac {
  const [seeds, _, ...tail] = lines;
  return {
    seeds: seeds.slice('seeds: '.length).split(' ').map(Number),
    maps: chunkBy(tail, line => !line).map(([_, ...mappings]) => {
      return mappings.map(mapping => {
        const [d, s, l] = mapping.split(' ').map(Number);
        return {
          destinationRangeStart: d,
          sourceRangeStart: s,
          rangeLength: l,
        };
      });
    }),
  };
}

const almanac = parseAlmanac(lines);

function getLocation(seed: number): number {
  let current = seed;

  almanac.maps.forEach(map => {
    for (const mapping of map) {
      if (
        current >= mapping.sourceRangeStart &&
        current < mapping.sourceRangeStart + mapping.rangeLength
      ) {
        current = mapping.destinationRangeStart + (current - mapping.sourceRangeStart);
        break;
      }
    }
  });

  return current;
}

const locations = almanac.seeds.map(getLocation);

console.log(Math.min(...locations));
